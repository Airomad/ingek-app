import React, { Component } from 'react';
import { Provider } from 'react-redux';

import routs from 'routs';
import store from 'store';

function App() {
  return (
    <Provider store={store}>
      {routs}
    </Provider>
  );
}

console.disableYellowBox = true;

export default routs;

// import AuthScreenContainer from 'containers/AuthScreenContainer';

// export default AuthScreenContainer;