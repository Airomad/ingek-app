import { AppRegistry, YellowBox } from 'react-native';
import App from './App';

// Temp. Some react library uses deprecated method
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

AppRegistry.registerComponent('ingekApp', () => App);
