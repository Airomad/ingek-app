import React, { Component } from 'react';
import { PixelRatio, Image, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import dict from 'utils/dictionary';

import photoUploadImg from 'icons/icon-profile-photo-upload.png';

import HeaderGradient from './HeaderGradient';
import HeaderNavigation from 'components/HeaderNavigation';


const HeaderContainer = styled.View`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(101.92)};
  justify-content: flex-start;
  align-items: center;
`;

const HeaderTitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.96)};
  font-weight: 600;
  text-align: center;
  color: #320f37;
`;

const PhotoWrapper = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  align-items: center;
`;

const PhotoContainer = styled.View`
  width: ${PixelRatio.getPixelSizeForLayoutSize(53.44)};
  height: ${PixelRatio.getPixelSizeForLayoutSize(53.44)};
  border-radius: ${PixelRatio.getPixelSizeForLayoutSize(27.22)};
  background: #ffffff;
  justify-content: center;
  align-items: center;
`;

const NameContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(7.04)};
`;

const NameLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(9.6)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
`;

const EditLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.76)};
  text-align: left;
  color: #cdcdcd;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(1)};
`;

const RoleLabel = styled.Text`
  justify-content: flex-start;
  align-items: center;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 300;
  text-align: center;
  color: #cdcdcd;
`;

const PhotoEditOverlay = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
`;

export default class ProfileHeader extends Component {
  renderUploadPhotoBtn = (imageSize, imageBorderRadius, isLoading) => (
    <PhotoEditOverlay>
      <TouchableOpacity
        onPress={this.props.onOpenPhoto}
        activeOpacity={0.7}
        disabled={isLoading}
        style={{
          backgroundColor: 'rgba(128, 25, 99, 0.8)',
          borderRadius: imageBorderRadius,
          width: imageSize,
          height: imageSize,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        {isLoading &&
          <ActivityIndicator size="small" color="#ffffff" />
        }
        {!isLoading &&
          <Image
            source={photoUploadImg}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(10.72),
              height: PixelRatio.getPixelSizeForLayoutSize(13.44),
            }}
          />
        }
      </TouchableOpacity>
    </PhotoEditOverlay>
  );

  renderNameRoleContainers = (name, role) => {
    const { editable } = this.props;

    if (!editable) {
      return [
        <NameContainer key="profile-name">
          <View style={{ flex: 1 }} />
          <NameLabel>{name}</NameLabel>
          <View style={{
            flex: 1,
            paddingTop: PixelRatio.getPixelSizeForLayoutSize(2.2),
            paddingLeft: PixelRatio.getPixelSizeForLayoutSize(3.2),
          }}>
          <TouchableOpacity
            onPress={this.props.onEdit}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(12),
              height: PixelRatio.getPixelSizeForLayoutSize(8),
            }}
          >
            <EditLabel>{dict.get('PROFILE_EDIT')}</EditLabel>
          </TouchableOpacity>
          </View>
        </NameContainer>,
  
        <RoleLabel key="profile-role">{role}</RoleLabel>
      ];
    }
  }

  render() {
    const { editable, image, name, role, onApply, onGoBack, onMenu, onOpenPhoto, photoUploading, applyDisabled } = this.props;
    const imageSize = PixelRatio.getPixelSizeForLayoutSize(42.24);
    const imageBorderRadius = PixelRatio.getPixelSizeForLayoutSize(27.22);

    return [
      <HeaderContainer key="profile-image">
        <HeaderGradient />

        <HeaderNavigation
          type={editable ? "operation" : "default"}
          onGoBack={onGoBack}
          onMenu={onMenu}
          onApply={onApply}
          applyDisabled={applyDisabled}
        />

        <PhotoWrapper>
          <PhotoContainer>
            <Image
              source={{ uri: image && image.length > 0 ? image : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg' }}
              style={{ width: imageSize, height: imageSize, borderRadius: imageBorderRadius, }}
            />
            {editable &&
              this.renderUploadPhotoBtn(imageSize, imageBorderRadius, photoUploading)
            }
          </PhotoContainer>
        </PhotoWrapper>
      </HeaderContainer>,

      this.renderNameRoleContainers(name, role),
    ];
  }
}
