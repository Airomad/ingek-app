import React, { Component } from 'react';
import {
  PixelRatio,
  Image,
  View,
  TouchableOpacity,
} from 'react-native';

import styled from 'styled-components/native';

import editImg from 'icons/icon-profile-edit.png';
import logoutImg from 'icons/icon-profile-logout.png';

const Wrapper = styled.View`
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(28.8)};
`;

export const bottomButtonsContainerHeight = PixelRatio.getPixelSizeForLayoutSize(22.56);

const BottomButtonsContainer = styled.View`
  height: ${bottomButtonsContainerHeight};
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
  background-color: #ffffff;
`;

export default class ProfileBottomButtons extends Component {
  render() {
    const { onEdit, onLogout } = this.props;
    const bottomIconSize = PixelRatio.getPixelSizeForLayoutSize(10.56);
    
    return (
      <BottomButtonsContainer>
        <TouchableOpacity
          onPress={onEdit}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            source={editImg}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(11.2),
              height: bottomIconSize,
            }}
          />
        </TouchableOpacity>
        
        <View style={{ width: 1, backgroundColor: '#e8e8e8' }} />

        <TouchableOpacity
          onPress={onLogout}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            source={logoutImg}
            style={{
              width: bottomIconSize,
              height: bottomIconSize,
            }}
          />
        </TouchableOpacity>
      </BottomButtonsContainer>
    );
  }
}
