import React, { Component } from 'react';
import {
  PixelRatio,
  Image,
  NativeModules,
  Platform,
  ScrollView,
  Dimensions,
  View,
  TouchableOpacity,
  Keyboard,
  ActivityIndicator,
} from 'react-native';

import styled from 'styled-components/native';
import { Header } from 'react-navigation';
import ImagePicker from 'react-native-image-picker';

import dict from 'utils/dictionary';

import editImg from 'icons/icon-profile-edit.png';
import logoutImg from 'icons/icon-profile-logout.png';

import ProfileHeader from './ProfileHeader';
import ProfileDetailsForm from './ProfileDetailsForm';
import ProfileBottomButtons, { bottomButtonsContainerHeight } from './ProfileBottomButtons';

import {
  Wrapper,
} from './ProfileScreen';


export default class ProfileEditScreen extends Component {
  state = {
    isBottomButtonsVisible: true,
    avatarSource: null,
  };

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow = () => {
    this.setState({ isBottomButtonsVisible: false });
  }

  keyboardDidHide = () => {
    this.setState({ isBottomButtonsVisible: true });
  }

  openPhoto = () => {
    console.log('running photo picker');
    var options = {
      title: dict.get('PROFILE_SELECT_PHOTO_TITLE'),
      // customButtons: [
      //   {name: 'fb', title: 'Choose Photo from Facebook'},
      // ],
      storageOptions: {
        skipBackup: true,
        path: 'images'
      }
    };
    
    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info below in README)
     */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      //console.log(response.data);
    
      if (response.didCancel) {
        console.log('User cancelled image picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        let source = { uri: response.uri };
    
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        
        this.props.onPrepareNewPhotoSource(source);
        this.setState({
          avatarSource: source
        });
      }
    });
  }

  render() {
    const { navigation, onApplyChangeUserData, onChangeUserData, photoUploading, alert, loading, faculties, groups } = this.props;
    const { isBottomButtonsVisible } = this.state;
    let { data } = this.props;
    if (!data) {
      data = {
        photo: '',
        fname: '',
        lname: '',
        type: '',
        login: '',
        password: '',
        sex: '',
        date_born: '',
        faculty: '',
        group: '',
      };
    }
    ///console.log('DDDAATTA', data);
    const { photo, fname, lname, type, login, password, sex, date_born, faculty, group } = data;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    const OTHER_BLOCKS_HEIGHT = STATUSBAR_HEIGHT + bottomButtonsContainerHeight + 1;
    const studentTypeLabels = {
      student: dict.get('USER_TYPE_STUDENT'),
      f_student: dict.get('USER_TYPE_FOREIGN'),
      teacher: dict.get('USER_TYPE_TEACHER'),
      enrolle: dict.get('USER_TYPE_ENROLEE'),
    };
    
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ScrollView
          contentContainerStyle={{
            minHeight: 640 - OTHER_BLOCKS_HEIGHT,
            backgroundColor: '#ffffff',
          }}
        >
          <Wrapper>
            <ProfileHeader
              image={this.state.avatarSource ? this.state.avatarSource.uri : photo}
              photoUploading={photoUploading}
              name={`${fname} ${lname}`}
              role={studentTypeLabels[type]}
              editable={true}
              onOpenPhoto={this.openPhoto}
              onGoBack={() => navigation.goBack()}
              onApply={onApplyChangeUserData}
              onMenu={() => navigation.toggleDrawer({
                side: 'left',
                animated: true,
              })}
              applyDisabled={photoUploading || loading}
            />
            {!loading &&
              <ProfileDetailsForm
                editable={true}
                data={data}
                faculties={faculties}
                groups={groups}
                onChangeData={onChangeUserData}
              />
            }
            {loading &&
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', height: 300 }}>
                <ActivityIndicator
                  size="large"
                  color="#46264a"
                />
              </View>
            }
          </Wrapper>
        </ScrollView>

        <View style={{ height: 1, backgroundColor: '#e8e8e8' }} />
        
        {isBottomButtonsVisible &&
          <ProfileBottomButtons
            onEdit={() => {}}
            onLogout={() => navigation.navigate('logout')}
          />
        }

        {alert && alert()}
      </View>
    );
  }
}
