import React, { Component } from 'react';
import { PixelRatio, Dimensions } from 'react-native';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Circle,
  Path
} from 'react-native-svg';

const Wrapper = styled.View`
  width: 100%;
  height: ${({ height }) => height || PixelRatio.getPixelSizeForLayoutSize(84.16)};
`;

export default function HeaderGradient() {
  const height = PixelRatio.getPixelSizeForLayoutSize(84.16);
  const screenWidth = PixelRatio.getPixelSizeForLayoutSize(Dimensions.get('window').width);
  const circleRadius = screenWidth * 0.8207;
  const cy = height - circleRadius;
  
  return (
    <Wrapper height={height}>
      <Svg width="100%" height={height}>
      <Circle
          cx="50%"
          cy={cy}
          r={circleRadius}
          fill="url(#paint0_linear)"
        />
        <Defs>
          <LinearGradient id="paint0_linear" x1="0%" y1="0%" x2="100%" y2="0%">
            <Stop offset="19%" stopColor="#331B49"/>
            <Stop offset="81%" stopColor="#FA188B"/>
          </LinearGradient>
        </Defs>

      </Svg>
    </Wrapper>
  );
}
