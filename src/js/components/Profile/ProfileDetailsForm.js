import React, { Component } from 'react';
import {
  PixelRatio,
  Image,
  TouchableOpacity,
  View,
  TextInput,
  Text,
} from 'react-native';

import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import dict from 'utils/dictionary';

import DateTimePicker from 'react-native-modal-datetime-picker';
import RNPickerSelect from 'react-native-picker-select';
import HeaderGradient from './HeaderGradient';

const offsetSide = PixelRatio.getPixelSizeForLayoutSize(14.4);

const TitleLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 600;
  text-align: left;
  color: #320f37;
  padding-left: ${offsetSide};
  padding-right: ${offsetSide};
  margin-top: ${({ marginTop }) => marginTop || 0};
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(6.24)};
`;

const InputContainer = styled.View`
  height: ${PixelRatio.getPixelSizeForLayoutSize(16.32)};
  background-color: ${({ editable }) => (editable ? '#f1f1f1' : 'transparent')};
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  margin-left: ${offsetSide};
  margin-right: ${offsetSide};
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(6.72)};
  padding-left: ${({ editable }) => editable ? PixelRatio.getPixelSizeForLayoutSize(5.76) : 0};
`;

const inputNamePadding = PixelRatio.getPixelSizeForLayoutSize(5.76);
const InputNameLabel = styled.Text`
  width: ${({ editable }) => (editable ? 'auto' : PixelRatio.getPixelSizeForLayoutSize(36.64))};
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
  font-weight: 600;
  text-align: left;
  color: ${({ editable }) => (editable ? '#b1b1b1' : '#691a5b')};
  margin-right: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
`;

function prepareReadonlyString(value) {
  if (value.length > 25) {
    return value.substr(0, 25) + '...';
  }
  return value;
}

function InputTextInner({ secureTextEntry, onChangeText, text, editable }) {
  return (
    <TextInput
      style={{
        flex: 1,
        color: (editable ? '#320f37' : '#9c9c9c'),
        textAlign: 'right',
        fontSize: PixelRatio.getPixelSizeForLayoutSize(5.76),
        paddingRight: PixelRatio.getPixelSizeForLayoutSize(4.96),
        // borderWidth: 1,
        // borderColor: 'blue',
      }}
      secureTextEntry={secureTextEntry}
      underlineColorAndroid="transparent"
      multiline={false}
      onChangeText={(text) => onChangeText(text)}
      value={editable ? text : (text ? prepareReadonlyString(text) : '')}
      editable={editable}
    />
  );
}

function InputSelectInner({ secureTextEntry, onChangeValue, value, editable, items }) {
  const itemsData = items ? items : [{ label: 'Select an item', value: null }];
  console.log(itemsData);
  //console.log('ITEMS IS', itemsData);
  let valueLabel = value;
  if (itemsData.length > 0) {
    const i = itemsData.filter(item => item.value === value);
    if (i && i[0]) {
      valueLabel = i[0].label;
    }
  }
  return (
    <RNPickerSelect
      //items={[{label: 'one', value: 1}, {label: 'two', value: 2}]}
      items={itemsData}
      placeholder={{}}
      onValueChange={onChangeValue}
      style={{ headlessAndroidContainer: { flex: 1 }}}
      disabled={!editable}
    >
      <View
        style={{
          flex: 1,
          //borderWidth: 1,
          //borderColor: 'blue',
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            color: (editable ? '#320f37' : '#9c9c9c'),
            textAlign: 'right',
            fontSize: PixelRatio.getPixelSizeForLayoutSize(5.76),
            paddingRight: PixelRatio.getPixelSizeForLayoutSize(4.96),
           // borderWidth: 1,
           // borderColor: 'red',
          }}
        >
          {valueLabel}
        </Text>
      </View>
    </RNPickerSelect>
  )
}

class InputDateInner extends Component {
  state = {
    isDatePickerVisible: false,
  };

  showDatePicker = () => this.setState({ isDatePickerVisible: true });

  hideDatePicker = () => this.setState({ isDatePickerVisible: false });

  prepareDateString = (date) => {
    const dateArr = date.toString().split(' ');
    const monthStr = dateArr[1].toLowerCase();
    let month;
    if (monthStr === 'jan') {
      month = '01';
    }
    if (monthStr === 'feb') {
      month = '02';
    }
    if (monthStr === 'mar') {
      month = '03';
    }
    if (monthStr === 'apr') {
      month = '04';
    }
    if (monthStr === 'may') {
      month = '05';
    }
    if (monthStr === 'jun') {
      month = '06';
    }
    if (monthStr === 'jul') {
      month = '07';
    }
    if (monthStr === 'aug') {
      month = '08';
    }
    if (monthStr === 'sep') {
      month = '09';
    }
    if (monthStr === 'oct') {
      month = '10';
    }
    if (monthStr === 'nov') {
      month = '11';
    }
    if (monthStr === 'dec') {
      month = '12';
    }

    return `${dateArr[3]}-${month}-${dateArr[2]}`;
  }

  handleDatePicked = (date) => {
    const preparedDate = this.prepareDateString(date);
    this.hideDatePicker();

    const { onChangeValue } = this.props;
    onChangeValue && onChangeValue(preparedDate);
  };

  render() {
    const { isDatePickerVisible } = this.state;
    const { value, editable } = this.props;
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
        }}
      >
        <TouchableOpacity
          onPress={this.showDatePicker}
          activeOpacity={0.6}
          style={{
            // borderWidth: 1,
            // borderColor: 'green',
            flex: 1,
            justifyContent: 'center',
          }}
        >
          <Text
            style={{
              color: (editable ? '#320f37' : '#9c9c9c'),
              textAlign: 'right',
              fontSize: PixelRatio.getPixelSizeForLayoutSize(5.76),
              paddingRight: PixelRatio.getPixelSizeForLayoutSize(4.96),
            // borderWidth: 1,
            // borderColor: 'red',
            }}
          >
            {value}
          </Text>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDatePicker}
        />
      </View>
    )
  }
}

function InputText(props) {
  if (props.editable) {
    return <InputTextInner {...props} />;
  } else {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}
      >
        <InputTextInner {...props} />
        <View style={{ height: 1, backgroundColor: '#e8e8e8' }} />
      </View>
    );
  }
}

function InputSelect(props) {
  if (props.editable) {
    return <InputSelectInner {...props} />;
  } else {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}
      >
        <InputSelectInner {...props} />
        <View style={{ height: 1, backgroundColor: '#e8e8e8' }} />
      </View>
    );
  }
}

function InputDate(props) {
  if (props.editable) {
    return <InputDateInner {...props} />;
  } else {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}
      >
        <InputTextInner {...props} />
        <View style={{ height: 1, backgroundColor: '#e8e8e8' }} />
      </View>
    );
  }
}

export default class ProfileDetailsForm extends Component {
  // state = {
  //   form: this.props.data,
  // };

  // componentWillReceiveProps(nextProps) {
  //   if (nextProps.data !== this.state.form) {
  //     this.setState({ form: nextProps.data });
  //   }
  // }

  changeFormValue = (inputName, value) => this.props.onChangeData && this.props.onChangeData(inputName, value);
  //this.props.onChangeUserData && this.props.onChangeUserData({ ...this.props.data, [inputName]: value }); 

  renderInputField = (inputType, name, displayName, value, isSecure, isEditable, listData) => (
    <InputContainer
      key={`profile-form-${name}`}
      editable={isEditable}
    >
      <InputNameLabel
        numberOfLines={1}
        editable={isEditable}
      >
        {displayName}
      </InputNameLabel>
      {inputType === 'input' &&
        <InputText
          text={value}
          onChangeText={(text) => this.changeFormValue(name, text)}
          editable={isEditable}
          secureTextEntry={isSecure}
        />
      }
      {inputType === 'select' &&
        <InputSelect
          value={value}
          items={listData}
          onChangeValue={(text) => this.changeFormValue(name, text)}
          editable={isEditable}
          secureTextEntry={isSecure}
        />
      }
      {inputType === 'date' &&
        <InputDate
          value={value}
          onChangeValue={(text) => this.changeFormValue(name, text)}
          editable={isEditable}
        />
      }
    </InputContainer>
  );

  renderInputNameRole = (fname, lname, type, editable) => {
    if (editable) {
      const userTypesListData = [
        {label: dict.get('USER_TYPE_STUDENT'), value: 'student'},
        {label: dict.get('USER_TYPE_FOREIGN'), value: 'f_student'},
        // {label: dict.get('USER_TYPE_TEACHER'), value: 'teacher'},
        {label: dict.get('USER_TYPE_ENROLEE'), value: 'enrolle'},
      ];
      return (
        <View key="profile-form-namerole" style={{ marginTop: PixelRatio.getPixelSizeForLayoutSize(33.28) }}>
          { this.renderInputField('input', 'fname', dict.get('PROFILE_FNAME'), fname, false, editable) }
          { this.renderInputField('input', 'lname', dict.get('PROFILE_SNAME'), lname, false, editable) }
          { this.renderInputField('select', 'type', dict.get('PROFILE_TYPE'), type, false, editable, userTypesListData) }
        </View>
      );
    }
  }

  render() {
    const { editable, faculties, groups } = this.props;
    //console.log('DATA IS');
    //console.log(this.props.data);
    const { fname, lname, type, login, password, sex, date_born, faculty, group } = this.props.data;
    const titleMarginTop = PixelRatio.getPixelSizeForLayoutSize(editable ? 11.04 : 59.68);
    const sexData = [
      { label: dict.get('SEX_MALE'), value: 'male' },
      { label: dict.get('SEX_FEMALE'), value: 'female' }
    ];
    //const sexValue = editable ? sex : (sex === 'male' ? dict.get('SEX_MALE') : sex === 'female' ? dict.get('SEX_FEMALE') : dict.get('null'));
    return [
      this.renderInputNameRole(fname, lname, type, editable),

      <TitleLabel
        key="profile-form-details"
        marginTop={titleMarginTop}
      >
        {dict.get('PROFILE_PRIVATE_DETAILS')}
      </TitleLabel>,

      this.renderInputField('input', 'login', dict.get('PROFILE_EMAIL'), login, false, editable),
      editable && this.renderInputField('input', 'password', dict.get('PROFILE_PASSWORD'), password, true, editable),
      this.renderInputField('select', 'sex', dict.get('PROFILE_SEX'), sex, false, editable, sexData),
      this.renderInputField('date', 'date_born', dict.get('PROFILE_DATE_BORN'), date_born, false, editable),
      this.renderInputField('select', 'faculty', dict.get('PROFILE_FACULTY'), faculty, false, editable, faculties),
      this.renderInputField('select', 'group', dict.get('PROFILE_GROUP'), group, false, editable, groups),
    ];
  }
}
