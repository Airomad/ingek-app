import React, { Component } from 'react';
import {
  PixelRatio,
  Image,
  NativeModules,
  Platform,
  ScrollView,
  Dimensions,
  View,
  TouchableOpacity,
} from 'react-native';

import dict from 'utils/dictionary';

import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import editImg from 'icons/icon-profile-edit.png';
import logoutImg from 'icons/icon-profile-logout.png';

import ProfileHeader from './ProfileHeader';
import ProfileDetailsForm from './ProfileDetailsForm';
import ProfileBottomButtons from './ProfileBottomButtons';

export const Wrapper = styled.View`
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(28.8)};
`;

const bottomButtonsContainerHeight = PixelRatio.getPixelSizeForLayoutSize(22.56);
const BottomButtonsContainer = styled.View`
  height: ${bottomButtonsContainerHeight};
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;


export default class ProfileScreen extends Component {
  goEdit = () => {
    this.props.navigation.navigate('profileEdit')
  }

  render() {
    const { navigation } = this.props;
    let { data } = this.props;
    if (!data) {
      data = {
        photo: '',
        fname: '',
        lname: '',
        type: '',
        login: '',
        password: '',
        sex: '',
        date_born: '',
        faculty: '',
        group: '',
      };
    }
    const { photo, fname, lname, type, login, password, sex, date_born, faculty, group } = data;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    const OTHER_BLOCKS_HEIGHT = STATUSBAR_HEIGHT + bottomButtonsContainerHeight + 1;
    const bottomIconSize = PixelRatio.getPixelSizeForLayoutSize(10.56);
    const studentTypeLabels = {
      student: dict.get('USER_TYPE_STUDENT'),
      f_student: dict.get('USER_TYPE_FOREIGN'),
      teacher: dict.get('USER_TYPE_TEACHER'),
      enrolle: dict.get('USER_TYPE_ENROLEE'),
    };
    
    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <ScrollView
          contentContainerStyle={{
            minHeight: 640 - OTHER_BLOCKS_HEIGHT,
            backgroundColor: '#ffffff',
          }}
        >
          <Wrapper>
            <ProfileHeader
              image={photo}
              name={`${fname} ${lname}`}
              role={studentTypeLabels[type]}
              editable={false}
              onEdit={this.goEdit}
              onGoBack={() => navigation.navigate('news')}
              onApply={() => navigation.navigate('profile')}
              onMenu={() => navigation.toggleDrawer({
                side: 'left',
                animated: true,
              })}
            />
            <ProfileDetailsForm
              editable={false}
              data={data}
            />
          </Wrapper>
        </ScrollView>

        <View style={{ height: 1, backgroundColor: '#e8e8e8' }} />
        
        <ProfileBottomButtons
          onEdit={this.goEdit}
          onLogout={() => navigation.navigate('logout')}
        />
      </View>
    );
  }
}
