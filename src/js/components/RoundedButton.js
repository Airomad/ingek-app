import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

const Container = styled.View`
  flex: 1;
  width: 100%;
`;

const LabelContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  font-family: SFUIText;
  font-size: ${({ size }) => PixelRatio.getPixelSizeForLayoutSize(size)};
  font-weight: 500;
  text-align: center;
  color: ${({ color }) => color};
`;

const LabelBold = Label.extend`
  font-weight: 500;
`;

const ButtonType2Container = styled.View`
  position: absolute;
  left: 1;
  top: 1;
  bottom: 1;
  right: 1;
  background-color: #ffffff;
  border-radius: 19;
`;

export default class RoundedButton extends Component {
  render() {
    const { text, text2, onPress, buttonType, style, disabled, loading } = this.props;
    const buttonHeight = PixelRatio.getPixelSizeForLayoutSize(24.48);
    const labelColor = buttonType === 'type2' ? '#320f37' : '#ffffff';
    const styles = {
      width: '100%',
      height: buttonHeight,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 20,
      ...style,
    }

    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.7}
        style={styles}
        disabled={disabled || loading}
      >
        <Container>
          <Svg
            width="100%"
            height={buttonHeight}
            preserveAspectRatio="none"
            style={{ flex: 1, padding: 2, opacity: loading ? 0.3 : 1 }}
          >
            <Defs>
              <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
                <Stop offset="0%" stopColor="#491a50" />
                <Stop offset="100%" stopColor="#e41884" />
              </LinearGradient>
            </Defs>
            
            <Rect
              width="100%"
              height="100%"
              fill={disabled ? '#989898' : 'url(#pbbg)'}
              rx={20}
              ry={20}
            />
          </Svg>

          {buttonType === 'type2' &&
            <ButtonType2Container>
            </ButtonType2Container>
          }

          <LabelContainer>
            {!loading &&
              <Label
                color={labelColor}
                size={buttonType === 'type3' ? 5.76 : 7.2}
              >
                {text}
                {buttonType === 'type3' &&
                  <LabelBold color={labelColor} size={7.2}> {text2}</LabelBold>
                }
              </Label>
            }
            {loading &&
              <ActivityIndicator size="small" color="#320f37" />
            }
          </LabelContainer>
        </Container>
      </TouchableOpacity>
    );
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
    text2: PropTypes.string,
    onPress: PropTypes.func.isRequired,
    buttonType: PropTypes.string,
    style: PropTypes.any,
    disabled: PropTypes.bool,
  };

  static defaultProps = {
    text2: null,
    buttonType: 'type1',
    style: null,
    disabled: false,
  };
}
