import React, { Component } from 'react';
import { PixelRatio, TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

const Container = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

const Label = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.32)};
  text-align: center;
  color: ${({ disabled }) => disabled ? '#939393' : '#320f37'};
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(9.24)};
`;

export default function BackFooterButton({ onGoHome, disabled }) {
  return (
    <Container>
      <TouchableOpacity
        onPress={onGoHome}
        activeOpacity={0.6}
        style={{
          padding: PixelRatio.getPixelSizeForLayoutSize(5),
        }}
      >
        <Label disabled={disabled}>{dict.get('BACK_FOOTER_BUTTON_LABEL')}</Label>
      </TouchableOpacity>
    </Container>
  );
}