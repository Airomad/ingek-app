import React, { Component } from 'react';
import { PixelRatio, Image, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import backImg from 'icons/icon-header-back-arrow.png';
import menuImg from 'icons/icon-header-menu.png';
import backDarkImg from 'icons/icon-header-back-arrow-dark.png';
import menuDarkImg from 'icons/icon-header-menu-dark.png';

const ButtonsContainer = styled.View`
  position: absolute;
  left: 0;
  top: ${PixelRatio.getPixelSizeForLayoutSize(26.08)};
  right: 0;
  flex-direction: row;
  justify-content: center;
  align-items: stretch;
`;

const ApplyLabel = styled.Text`
  font-family: SFUIText;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 600;
  text-align: right;
  color: ${({ theme }) => theme === 'light' ? '#ffffff' : '#320f37'};
`;

const ApplyLabelDisabled = ApplyLabel.extend`
  color: ${({ theme }) => theme === 'light' ? '#bcbcbc' : '#bcbcbc'};
`;

const TitleLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.96)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
`;

const TYPE_DEFAULT = 'default';
const TYPE_OPERATION = 'operation';
const TYPE_JUST_BACK = 'just-back';
const TYPE_FINISH_OPERATION = 'finish-operation';
const TYPE_NEXT_OPERATION = 'next-operation';
const THEME_LIGHT = 'light';
const THEME_DARK = 'dark';

export default class HeaderNavigation extends Component {
  static propTypes = {
    type: PropTypes.oneOf([TYPE_DEFAULT, TYPE_OPERATION, TYPE_JUST_BACK, TYPE_FINISH_OPERATION, TYPE_NEXT_OPERATION]),
    theme: PropTypes.oneOf([THEME_LIGHT, THEME_DARK]),
    onGoBack: PropTypes.func,
    onMenu: PropTypes.func,
    onApply: PropTypes.func,
  };

  static defaultProps = {
    type: TYPE_DEFAULT,
    theme: THEME_LIGHT,
  };

  constructor(props) {
    super(props);
  }

  renderRightButtonContent = () => {
    const { type, theme, applyDisabled } = this.props;

    switch (type) {
      case TYPE_JUST_BACK:
        return null;

      case TYPE_OPERATION:
        if (!applyDisabled) {
          return (
            <ApplyLabel numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_APPLY')}</ApplyLabel>
          );
        } else {
          return (
            <ApplyLabelDisabled numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_APPLY')}</ApplyLabelDisabled>
          );
        }

      case TYPE_FINISH_OPERATION:
        if (!applyDisabled) {
          return (
            <ApplyLabel numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_DONE')}</ApplyLabel>
          );
        } else {
          return (
            <ApplyLabelDisabled numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_DONE')}</ApplyLabelDisabled>
          );
        }

      case TYPE_NEXT_OPERATION:
        if (!applyDisabled) {
          return (
            <ApplyLabel numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_NEXT')}</ApplyLabel>
          );
        } else {
          return (
            <ApplyLabelDisabled numberOfLines={1} theme={theme}>{dict.get('HEADER_NAVIGATION_NEXT')}</ApplyLabelDisabled>
          );
        }

      case TYPE_DEFAULT: default:
        return (
          <Image
            source={theme === THEME_LIGHT ? menuImg : menuDarkImg}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(9.76),
              height: PixelRatio.getPixelSizeForLayoutSize(8.64),
            }}
          />
        );
    }
  }

  render() {
    const { type, onApply, onGoBack, onMenu, theme, title, applyDisabled } = this.props;

    return (
      <ButtonsContainer>
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', }}>
          <TouchableOpacity
            onPress={onGoBack}
            style={{
              padding: PixelRatio.getPixelSizeForLayoutSize(3.2),
              marginLeft: PixelRatio.getPixelSizeForLayoutSize(10.88),
            }}
          >
            <Image
              source={theme === THEME_LIGHT ? backImg : backDarkImg}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(12.64),
                height: PixelRatio.getPixelSizeForLayoutSize(8.64),
              }}
            />
          </TouchableOpacity>
        </View>
        {title &&
          <View style={{ flex: 2, justifyContent: 'center', alignItems: 'center' }}>
            <TitleLabel numberOfLines={1}>{title.toUpperCase()}</TitleLabel>
          </View>
        }
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', }}>
          <TouchableOpacity
            onPress={type === TYPE_OPERATION || type === TYPE_FINISH_OPERATION || type === TYPE_NEXT_OPERATION ? onApply : onMenu}
            style={{
              padding: PixelRatio.getPixelSizeForLayoutSize(3.2),
              marginRight: PixelRatio.getPixelSizeForLayoutSize(10.88),
            }}
            disabled={applyDisabled}
          >
            { this.renderRightButtonContent() }
          </TouchableOpacity>
        </View>
      </ButtonsContainer>
    );
  }
}
