import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  PixelRatio,
} from 'react-native';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Circle,
} from 'react-native-svg';

const Label = styled.Text`
  flex: 1;
  justify-content: flex-start;
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(9.76)};
  font-weight: 500;
  text-align: left;
  color: ${({ active }) => active ? '#320f37' : '#989898'};
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(6.56)};
`;

export default class SignUpScreen extends Component {
  render() {
    const { text, active, style, onPress, labelStyle, withoutPaddingLeft } = this.props;
    const circleSize = PixelRatio.getPixelSizeForLayoutSize(14.88);

    return (
      <TouchableOpacity
        onPress={onPress}
        activeOpacity={0.7}
        style={{
          flexDirection: 'row',
          marginLeft: withoutPaddingLeft ? 0 : PixelRatio.getPixelSizeForLayoutSize(3.52),
          alignItems: 'center',
          ...style,
        }}
      >
        <Svg
          width={circleSize + 2}
          height={circleSize + 2}
        >
          <Defs>
            <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
              <Stop offset="0%" stopColor="#491a50" />
              <Stop offset="100%" stopColor="#e41884" />
            </LinearGradient>
          </Defs>
          <Circle
            cx={circleSize / 2 + 1}
            cy={circleSize / 2 + 1}
            r={circleSize / 2}
            fill="#ffffff"
            strokeWidth={1}
            stroke="url(#pbbg)"
          />
          {active &&
            <Circle
              cx={circleSize / 2 + 1}
              cy={circleSize / 2 + 1}
              r={PixelRatio.getPixelSizeForLayoutSize(5.88)}
              fill="url(#pbbg)"
            />
          }
        </Svg>
        <Label
          numberOfLines={1}
          active={active}
          style={{...labelStyle}}
        >
          {text}
        </Label>
      </TouchableOpacity>
    );
  }
}
