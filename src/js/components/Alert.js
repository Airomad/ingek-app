import React, { Component } from 'react';
import { PixelRatio, Animated } from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';


const Label = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7)};
  text-align: center;
  color: #ffffff;
`;

export default class HeaderNavigation extends Component {
  static propTypes = {
    
  };

  static defaultProps = {
    
  };

  state = {
    contentOpacity: new Animated.Value(1),
  };

  componentDidMount() {
    const { contentOpacity } = this.state;
    const { delay, onDisapear } = this.props;
    const animDelay = (delay - 1000 < 0) ? 0 : delay - 1000;

    Animated.sequence([
      Animated.delay(animDelay),
      Animated.timing(
        contentOpacity,
        {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }
      )
    ]).start(onDisapear);
  }

  render() {
    const paddingSide = PixelRatio.getPixelSizeForLayoutSize(15);
    const { contentOpacity } = this.state;
    const { message } = this.props;

    return (
      <Animated.View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#AF1A1A',
          height: PixelRatio.getPixelSizeForLayoutSize(14),
          paddingLeft: paddingSide,
          paddingRight: paddingSide,
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          opacity: contentOpacity,
        }}
      >
        <Label numberOfLines={1}>{message}</Label>
      </Animated.View>
    );
  }
}
