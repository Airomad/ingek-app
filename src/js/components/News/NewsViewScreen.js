import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import Carousel, { Pagination } from 'react-native-snap-carousel';

import HeaderNavigation from 'components/HeaderNavigation';
import BackFooterButton from 'components/BackFooterButton';


const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const HeaderContainer = styled.View`
  height: ${Dimensions.get('window').width * 1.1775};
  background: #ffffff;
`;

const BulletsContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  bottom: ${PixelRatio.getPixelSizeForLayoutSize(3.52)};
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const sidePadding = PixelRatio.getPixelSizeForLayoutSize(13.44);

const DateLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(4.8)};
  font-weight: 500;
  text-align: center;
  color: #bcbcbc;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(15.68)};
  margin-bottom: 0;
  padding-left: ${sidePadding};
  padding-right: ${sidePadding};
`;

const TitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(6.4)};
  margin-bottom: 0;
  padding-left: ${sidePadding};
  padding-right: ${sidePadding};
`;

const TextContainer = styled.View`
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(20.64)};
  margin-bottom: 0;
  padding-left: ${sidePadding};
  padding-right: ${sidePadding};
`;

const Paragraph = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.76)};
  text-align: left;
  color: #898888;
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(11.84)};
`;

export default class NewsViewScreen extends Component {
  static propTypes = {
    data: PropTypes.shape({
      title: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      images: PropTypes.arrayOf(PropTypes.string).isRequired,
      text: PropTypes.arrayOf(PropTypes.string).isRequired,
    }),
  };
  
  state = {
    activeSlideIndex: 0,
  };

  renderText = (text) => {
    const lines = [];
    text.forEach((line, index) => {
      lines.push(<Paragraph key={`l${index}`}>{line}</Paragraph>);
    });
    return lines;
  }

  renderSliderItem = ({ item, index }) => {
    return (
      <Image
        source={{ uri: item }}
        style={{ width: '100%', height: '100%' }}
      />
    );
  }

  renderPage = (data) => {
    const { title, date, id, images, text } = data;
    const { navigation } = this.props;
    const { activeSlideIndex } = this.state;
    const bulletSide = PixelRatio.getPixelSizeForLayoutSize(3.84);
    const bulletMargin = PixelRatio.getPixelSizeForLayoutSize(1.12);

    return (
      <View>

        {images && images.length > 0 &&
          <HeaderContainer>

            <Carousel 
              ref={(c) => { this._carousel = c; }}
              data={images}
              renderItem={this.renderSliderItem}
              layout="default"
              inactiveSlideScale={1}
              inactiveSlideOpacity={1}
              loop={true}
              loopClonesPerSide={2}
              sliderWidth={Dimensions.get('window').width}
              itemWidth={Dimensions.get('window').width}
              onSnapToItem={(index) => this.setState({ activeSlideIndex: index }) }
              enableMomentum={false}
            />
            
            <BulletsContainer>
              <Pagination
                dotsLength={images.length}
                activeDotIndex={activeSlideIndex}
                dotContainerStyle={{
                  marginLeft: bulletMargin,
                  marginRight: bulletMargin,
                }}
                dotColor="#ffffff"
                dotStyle={{
                  width: bulletSide,
                  height: bulletSide,
                  marginLeft: 0,
                  marginRight: 0,
                }}
                inactiveDotColor="#ffffff"
                inactiveDotOpacity={0.5}
                inactiveDotScale={1}
                carouselRef={this._carousel}
                tappableDots={false}
              />
            </BulletsContainer>

          </HeaderContainer>
        }

        <HeaderNavigation
          type="default"
          theme={(images && images.length > 0) ? 'light' : 'dark'}
          onGoBack={() => navigation.goBack()}
          onMenu={() => navigation.toggleDrawer({
            side: 'left',
            animated: true,
          })}
        />

        {(!images || images.length === 0) &&
          <View style={{ height: 100 }} />
        }

        <DateLabel numberOfLines={1}>{date}</DateLabel>
        <TitleLabel numberOfLines={1}>{title}</TitleLabel>
        <TextContainer>{ this.renderText(text) }</TextContainer>
      </View>
    );
  }

  render() {
    const { navigation, data, loading } = this.props;
    const { StatusBarManager } = NativeModules;

    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    
    return (
      <ScrollView
        contentContainerStyle={{
          minHeight: 640 - STATUSBAR_HEIGHT,
        }}
      >
        <Wrapper>
          { loading &&
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="small" color="#320f37" />
            </View>
          }

          {data && !loading &&
            this.renderPage(data)
          }
          
          <BackFooterButton onGoHome={() => navigation.navigate('feed')} />
        </Wrapper>
      </ScrollView>
    );
  }
}
