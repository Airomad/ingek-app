import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import quoteImg from 'icons/icon-news-quote.png';
import BackFooterButton from 'components/BackFooterButton';
import HeaderNavigation from 'components/HeaderNavigation';


const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const PlateContainer = styled.View`
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  border: 1px solid #E5E5E5;
  flex-direction: row;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
  margin-right: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
`;

const PlateDateContainer = styled.View`
  width: ${PixelRatio.getPixelSizeForLayoutSize(36.48)};
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(11.92)};
`;

const DateLabel = styled.Text`
  font-family: ModernSans;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(18.16)};
  text-align: center;
  color: #320f37;
  margin-bottom: 0;
  margin-top: 0;
`;

const DayLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(4.4)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
  margin-top: 0;
  margin-bottom: 0;
`;

const YearLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.36)};
  font-weight: 500;
  text-align: center;
  color: #c1c1c1;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(7.04)};
  margin-bottom: 0;
`;

const ImageContainer = styled.View`
  flex: 1;
  height: ${PixelRatio.getPixelSizeForLayoutSize(75.04)};
`;

const textContainerPadding = PixelRatio.getPixelSizeForLayoutSize(7.36);
const TextContainer = styled.View`
  flex: 1;
  padding-top: ${textContainerPadding};
  padding-bottom: ${textContainerPadding};
`;

const paddingSide = PixelRatio.getPixelSizeForLayoutSize(4.8);
const QuoteContainer = styled.View`
  justify-content: center;
  align-items: flex-start;
  padding-left: ${paddingSide};
  padding-right: ${paddingSide};
  ${({ type }) => type === 'end' && `
    transform: rotate(180deg);
  `}
`;

const textPaddingSide = PixelRatio.getPixelSizeForLayoutSize(9.6);
const TextInnerContainer = styled.View`
  padding-left: ${textPaddingSide};
  padding-right: ${textPaddingSide};
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  padding-bottom: ${PixelRatio.getPixelSizeForLayoutSize(11.2)};
`;

const QuoteTitle = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.2)};
  font-weight: 500;
  text-align: left;
  color: #320f37;
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(6.56)};
`;

const QuoteText = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.76)};
  font-weight: 500;
  text-align: left;
  color: #c6c6c6;
`;

export default class NewsScreen extends Component {
  static propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      type: PropTypes.oneOf(['news', 'adv']).isRequired,
      date: PropTypes.shape({
        day: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
        month: PropTypes.string.isRequired,
        year: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
      }).isRequired,
      poster: PropTypes.string,
      text: PropTypes.shape({
        title: PropTypes.string.isRequired,
        lines: PropTypes.arrayOf(PropTypes.string).isRequired,
      }),
    })),
  };

  renderQuotedText = (title, lines) => {
    const quoteIconWidth = PixelRatio.getPixelSizeForLayoutSize(6.88);
    const quoteIconHeight = PixelRatio.getPixelSizeForLayoutSize(4.96);
    const textBlocks = [];

    lines.forEach((text, index) => {
      textBlocks.push(
        <QuoteText key={`l${index}`}>{text}</QuoteText>
      );
    });

    return [
      <QuoteContainer type="start" key="quote-start">
        <Image
          source={quoteImg}
          style={{
            width: quoteIconWidth,
            height: quoteIconHeight,
          }}
        />
      </QuoteContainer>,
      <TextInnerContainer key="quote-text">
        <QuoteTitle>{title}</QuoteTitle>
        { textBlocks }
      </TextInnerContainer>,
      <QuoteContainer type="end" key="quote-end">
        <Image
          source={quoteImg}
          style={{
            width: quoteIconWidth,
            height: quoteIconHeight,
          }}
        />
      </QuoteContainer>,
    ];
  }

  renderPoster = (uri) => {
    return (
      <ImageContainer>
        <Image
          source={{ uri: uri }}
          style={{ width: '100%', height: '100%' }}
        />
      </ImageContainer>
    );
  }

  renderItem = ({ item }) => {
    const { navigation } = this.props;
    const { type, id, text, day, month, year, poster } = item;
    if (type === 'news') {
      return (
        <TouchableOpacity
          onPress={() => navigation.navigate('newsView', { id })}
          activeOpacity={0.7}
          key={`neesp${id}`}
        >
          <PlateContainer>
            <PlateDateContainer>
              <DateLabel>{day}</DateLabel>
              <DayLabel>{month}</DayLabel>
              <YearLabel>{year}</YearLabel>
            </PlateDateContainer>

            <View style={{ width: 1, backgroundColor: '#e8e8e8', }} />

            {(text && poster) &&
              <View style={{ flex: 1, flexDirection: 'column' }}>
                <TextContainer>
                  { this.renderQuotedText(text.title, text.lines) }
                </TextContainer>
                { this.renderPoster(poster) }
              </View>
            }
            
            {(text && !poster) &&
              <TextContainer>
                { this.renderQuotedText(text.title, text.lines) }
              </TextContainer>
            }

            {(!text && poster) &&
              this.renderPoster(poster)
            }

          </PlateContainer>
        </TouchableOpacity>
      );
    } else {
      // TODO add render for ADV
      return null;
    }
  }

  renderHeader = () => {
    const { navigation } = this.props;
    return (
      <View style={{ height: 100 }}>
        <HeaderNavigation
          type="default"
          theme="dark"
          title={dict.get('NEWS_TITLE')}
          onGoBack={() => navigation.navigate('feed')}
          onMenu={() => navigation.toggleDrawer({
            side: 'left',
            animated: true,
          })}
        />
      </View>
    );
  }

  renderFooter = () => {
    const { loading, isLoadedAllData } = this.props;

    if (!loading && !isLoadedAllData) {
      return (
        <View
          style={{
            flex: 1,
            height: 200,
            // borderWidth: 1,
            // borderColor: 'red',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 20,
          }}
        >
          <ActivityIndicator size="small" color="#320f37" />
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    const { navigation, alert, data, loading, onRefreshPage, isLoadedAllData, onLoadMoreData } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    
    return (
      // <ScrollView
      //   contentContainerStyle={{
      //     minHeight: 640 - STATUSBAR_HEIGHT,
      //   }}
      // >
      //   <Wrapper>
          


      //     {loading &&
      //       <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      //         <ActivityIndicator size="small" color="#320f37" />
      //       </View>
      //     }

      //     {!loading &&
      //       this.renderPlates(data)
      //     }

      //     <BackFooterButton
      //       disabled
      //       onGoHome={() => navigation.navigate('feed')}
      //     />

      //   </Wrapper>
      // </ScrollView>

      <Wrapper>
        <FlatList
          data={data}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          keyExtractor={(item, index) => `newsk${index}`}
          renderItem={this.renderItem}
          onRefresh={onRefreshPage}
          refreshing={loading}
          onEndReachedThreshold={0.1}
          onEndReached={!isLoadedAllData ? onLoadMoreData : null}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
        />

        {alert && alert()}

      </Wrapper>
    );
  }
}
