import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
  WebView,
} from 'react-native';
import styled from 'styled-components/native';

import HeaderNavigation from 'components/HeaderNavigation';
import RoundedButton from 'components/RoundedButton';

import MapViewer from 'components/Navigator/MapViewer';

import logoImg from 'images/logo-big-schedule.png';

export const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const Label = styled.Text`
  color: black;
  font-size: 20px;
`;


export default class NavigatorScreen extends Component {
  static propTypes = {
    
  };

  render() {
    const { navigation, loading } = this.props;

    return (
      <Wrapper>

        <HeaderNavigation
          type="default"
          theme="dark"
          title="NAVIGATOR"
          onGoBack={() => navigation.navigate('news')}
          onMenu={() => navigation.toggleDrawer({
            side: 'left',
            animated: true,
          })}
        />

        {/* <MapViewer /> */}
        <View
          style={{
            flex: 1,
            borderWidth: 1,
            borderColor: 'green',
            marginTop: 100,
          }}
        >
          <WebView
              source = {{ uri: 'https://www.site3.hneu.edu.ua/app/navigator/index.html' }}
              style={{
                borderWidth: 1,
                borderColor: 'red',
              }}
          />
        </View>

      </Wrapper>
    );
  }
}
