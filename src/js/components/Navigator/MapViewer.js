import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import styled from 'styled-components/native';

import PinchZoomView from 'react-native-pinch-zoom-view';


export default class MapViewer extends Component {
  static propTypes = {
    
  };

  render() {
    return (
      // <ScrollView
      //   style={{
      //     flex: 1,
      //     borderWidth: 1,
      //     borderColor: 'blue',
      //   }}
      //   contentContainerStyle={{
      //     width: 1000,
      //     height: 1000,
      //   }}
      //   maximumZoomScale={10}
      //   minimumZoomScale={1}

      // >
      //   <View
      //     style={{
      //       position: 'absolute',
      //       left: 10,
      //       top: 100,
      //       width: 100,
      //       height: 50,
      //       backgroundColor: 'green',
      //     }}
      //   />
      // </ScrollView>
      <PinchZoomView>
        <View
          style={{
            position: 'absolute',
            left: 10,
            top: 100,
            width: 100,
            height: 50,
            backgroundColor: 'green',
          }}
        />
      </PinchZoomView>
    );
  }
}
