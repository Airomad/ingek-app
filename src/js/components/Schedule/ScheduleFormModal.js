import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

import dict from 'utils/dictionary';

import plusImg from 'icons/icon-schedule-plus.png';


const Wrapper = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ffffff;
`

const OpenButtonContainer = styled.View`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(27.68)};
  background: #ffffff;
  flex-direction: row;
  align-items: center;
  padding-left: ${PixelRatio.getPixelSizeForLayoutSize(13.76)};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(13.76)};
`;

const OpenLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  font-weight: 500;
  text-align: left;
  color: #c6c6c6;
  flex: 1;
`;

const OpenButtonLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  font-weight: 600;
  text-align: right;
  color: #46264a;
`;

const RowSeparator = styled.View`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(0.48)};
  background: #ededed;
`;

const RowScheduleButton = styled(TouchableOpacity)`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(27.68)};
  justify-content: center;
  align-items: center;
`;

const ScheduleButtonLabel = OpenButtonLabel.extend`
  width: 100%;
  padding-left: ${PixelRatio.getPixelSizeForLayoutSize(14.08)};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(14.08)};
  text-align: left;
`;

export default class ScheduleFormModal extends Component {
  state = {
    isOpened: false,
    // searchRequests: [
    //   { label: '23423424: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Group: 6.06.073.062.17.2', value: 123 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 },
    //   { label: 'Teacher: Litvin Olekandr Oleksandr', value: 234 }
    // ]
  };

  renderSearchRequests = () => {
    const { data } = this.props;
    const rows = [];

    if (data && data.length > 0) {
      data.forEach((item, index) => {
        rows.push(<RowSeparator key={Math.random()} />);
        rows.push(
          <RowScheduleButton
            key={`rs${index}`}
            onPress={() => {}}
          >
            <ScheduleButtonLabel numberOfLines={1}>{item.label}</ScheduleButtonLabel>
          </RowScheduleButton>
        );
      });
    }

    rows.push(<RowSeparator key={Math.random()} />);
    rows.push(
      <RowScheduleButton
        key="new-search-btn"
        onPress={() => {}}
      >
        <Image
          source={plusImg}
          style={{
            width: PixelRatio.getPixelSizeForLayoutSize(10.4),
            height: PixelRatio.getPixelSizeForLayoutSize(10.4),
          }}
        />
      </RowScheduleButton>
    );

    return (
      <ScrollView
        style={{
          maxHeight: 5 * PixelRatio.getPixelSizeForLayoutSize(28.16),
        }}
      >
        {rows}
      </ScrollView>
    );
  }

  render() {
    const { data } = this.props;
    const { isOpened } = this.state;

    return (
      <Wrapper>

        <Svg
          width="100%"
          height={PixelRatio.getPixelSizeForLayoutSize(4.8)}
          preserveAspectRatio="none"
          style={{ flex: 1 }}
        >
          <Defs>
            <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
              <Stop offset="0%" stopColor="#491a50" />
              <Stop offset="100%" stopColor="#e41884" />
            </LinearGradient>
          </Defs>
          
          <Rect
            width="100%"
            height="100%"
            fill="url(#pbbg)"
          />
        </Svg>

        <OpenButtonContainer>
          <OpenLabel numberOfLines={1}>{(data && data.length > 0) ? dict.get('SCHEDULE_SEARCH_SELECT') : dict.get('SCHEDULE_SEARCH_FIND')}</OpenLabel>
          <TouchableOpacity
            onPress={() => this.setState({ isOpened: !isOpened })}
            style={{
              height: '100%',
              marginLeft: PixelRatio.getPixelSizeForLayoutSize(8),
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <OpenButtonLabel>{isOpened ? dict.get('SCHEDULE_SEARCH_CLOSE').toUpperCase() : dict.get('SCHEDULE_SEARCH_OPEN').toUpperCase()}</OpenButtonLabel>
          </TouchableOpacity>
        </OpenButtonContainer>

        {isOpened && this.renderSearchRequests()}

      </Wrapper>
    );
  }

  static propTypes = {
    
  };
}
