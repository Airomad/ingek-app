import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import SearchInput from 'components/Schedule/SearchInput';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-color: #ffffff;
  flex: 1;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(52.24)};
  padding-bottom: ${PixelRatio.getPixelSizeForLayoutSize(13.6)};
`;


export default class ScheduleSearchScreen extends Component {
  static propTypes = {
    
  };

  render() {
    const {
      navigation,
      alert,
      autoCompleteData,
      loading,
      onSelectAutoCompleteVariant,
      onChangeSearchInputText,
      searchInputText
    } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          contentContainerStyle={{
            minHeight: 640 - STATUSBAR_HEIGHT,
          }}
        >
          <Wrapper>

            <HeaderNavigation
              type="default"
              theme="dark"
              title={dict.get('SCHEDULE_SEARCH_TITLE')}
              onGoBack={() => navigation.navigate('news')}
              onMenu={() => navigation.toggleDrawer({
                side: 'left',
                animated: true,
              })}
            />

            <SearchInput
              placeholder={dict.get('SCHEDULE_SEARCH_INPUT_SEARCH')}
              text={searchInputText}
              onChangeValue={onChangeSearchInputText}
              onSelectFoundVariant={onSelectAutoCompleteVariant}
              loading={loading}
              foundVariants={autoCompleteData}
            />

          </Wrapper>

          {alert && alert()}

        </ScrollView>

      </View>
    );
  }
}
