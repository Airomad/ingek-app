import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

import placeLightImg from 'icons/icon-place-light.png';


const Label = styled.Text`
  font-family: SFUIText;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.6)};
  font-weight: 500;
  text-align: center;
  color: #ffffff;
`;


export default class FindButton extends Component {
  render() {
    const { text } = this.props;

    return (
      <TouchableOpacity
        onPress={() => {}}
        activeOpacity={0.6}
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          height: PixelRatio.getPixelSizeForLayoutSize(16.8),
          backgroundColor: '#46264a',
          borderRadius: PixelRatio.getPixelSizeForLayoutSize(1.6),
        }}
      >
        <Image
          source={placeLightImg}
          style={{
            width: PixelRatio.getPixelSizeForLayoutSize(6.25),
            height: PixelRatio.getPixelSizeForLayoutSize(8.29),
            marginRight: PixelRatio.getPixelSizeForLayoutSize(2.88),
          }}
        />
        <Label numberOfLines={1}>{text && text.toUpperCase()}</Label>
      </TouchableOpacity>
    );
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
  };
}
