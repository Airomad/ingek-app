import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import RoundedButton from 'components/RoundedButton';
import logoImg from 'images/logo-big-schedule.png';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-color: #ffffff;
  flex: 1;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(42.04)};
  padding-bottom: ${PixelRatio.getPixelSizeForLayoutSize(13.6)};
`;

const LogoWrapper = styled.View`
  flex: 1;
  min-height: ${PixelRatio.getPixelSizeForLayoutSize(20)};
  border: 1px solid red;
  justify-content: center;
  align-items: center;
`;

const TitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
  padding-left: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(21.12)};
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(20.48)};
`;

const Text = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(5.76)};
  text-align: center;
  color: #898888;
`;

const Text2 = Text.extend`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(4.8)};
  color: #320f37;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(11.2)};
`;


export default class ScheduleLockedScreen extends Component {
  static propTypes = {
    
  };

  render() {
    const { navigation, loading, alert } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    const offsetSide = PixelRatio.getPixelSizeForLayoutSize(12.16);

    return (
        <Wrapper>
          
          <HeaderNavigation
            type="just-back"
            theme="dark"
            onGoBack={() => navigation.navigate('news')}
          />

          <ScrollView
            style={{ flex: 2, marginBottom: PixelRatio.getPixelSizeForLayoutSize(12.64) }}
            contentContainerStyle={{
              paddingLeft: offsetSide,
              paddingRight: offsetSide,
              justifyContent: 'flex-start',
              alignItems: 'center',
            }}
          >
          <Image
            source={logoImg}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(51.2),
              height: PixelRatio.getPixelSizeForLayoutSize(79.04),
            }}
          />

          <TitleLabel key="title">{dict.get('SCHEDULE_WELCOME')}</TitleLabel>
            <Text>{dict.get('SCHEDULE_LOCKED_TEXT1')}</Text>
            <Text2>{dict.get('SCHEDULE_LOCKED_TEXT2')}</Text2>
          </ScrollView>

          <View
            style={{
              height: PixelRatio.getPixelSizeForLayoutSize(40),
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              paddingLeft: offsetSide,
              paddingRight: offsetSide,
            }}
          >
            <RoundedButton
              onPress={() => navigation.navigate('schedule')}
              text={dict.get('SCHEDULE_UNLOCK')}
            />
          </View>

          {alert && alert()}

      </Wrapper>
    );
  }
}
