import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import searchImg from 'icons/icon-schedule-search.png';
import arrowImg from 'icons/icon-schedule-arrow.png';
import lupaImg from 'icons/icon-schedule-lupa.png';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-end;
`;

const InputContainer = styled.View`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(19.04)};
  background: #f6f6f6;
  justify-content: center;
  align-items: center;
`;

const PlaceholderLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.52)};
  font-weight: 600;
  text-align: center;
  color: #e0e0e0;
`;

const PlaceholderContainer = styled(TouchableOpacity)`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

const AdvancedSearchLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.4)};
  text-align: right;
  color: #939393;
`;

const FoundVariantsContainer = styled.View`
  width: 100%;
`;

const FoundVariantLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  font-weight: 600;
  text-align: left;
  color: #46264a;
`;


function Separator() {
  return (
    <View
      key={`sep${Math.random}`}
      style={{
        width: '100%',
        height: PixelRatio.getPixelSizeForLayoutSize(0.64),
        backgroundColor: '#ededed',
      }}
    />
  )
}


export default class SearchInput extends Component {
  state = {
    isInputFocused: false,
  }

  focusInput = () => {
    this.setState({ isInputFocused: true });
    this.searcInputRef.focus();
  }

  focusDisable = () => {
    this.setState({ isInputFocused: false });
  }

  renderFoundVariants = () => {
    const rows = [];
    const { loading, foundVariants, onSelectFoundVariant } = this.props;

    if (loading) {
      rows.push(
        <View
          key="search-input-loading"
          style={{
            width: '100%',
            height: PixelRatio.getPixelSizeForLayoutSize(27.2),
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <ActivityIndicator color="#46264a" size="small" />
        </View>
      );
      rows.push(<Separator />);
    } else {
      if (foundVariants && foundVariants.length > 0) {
        foundVariants.forEach((item, index) => {
          rows.push(
            <TouchableOpacity
              onPress={() => onSelectFoundVariant(item)}
              key={`siv${index}`}
              style={{
                width: '100%',
                height: PixelRatio.getPixelSizeForLayoutSize(27.2),
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
                paddingLeft: PixelRatio.getPixelSizeForLayoutSize(13.92),
                paddingRight: PixelRatio.getPixelSizeForLayoutSize(13.92),
              }}
            >
              <FoundVariantLabel numberOfLines={1}>{item.label}</FoundVariantLabel>
            </TouchableOpacity>
          );
          rows.push(<Separator />);
        });
      }
    }

    return (
      <FoundVariantsContainer>
        {rows}
      </FoundVariantsContainer>
    )
  }

  render() {
    const { isInputFocused } = this.state;
    const { placeholder, text, loading, onChangeValue } = this.props;

    return (
      <Wrapper>

        <InputContainer>
          {text.length < 1 && !isInputFocused &&
            <PlaceholderContainer onPress={this.focusInput}>
              <Image
                source={searchImg}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(8.64),
                  height: PixelRatio.getPixelSizeForLayoutSize(11.68),
                  marginRight: PixelRatio.getPixelSizeForLayoutSize(5.12),
                }}
              />
              <PlaceholderLabel>{placeholder}</PlaceholderLabel>
            </PlaceholderContainer>
          }
          <TextInput
            underlineColorAndroid="rgba(0,0,0,0)"
            ref={ref => this.searcInputRef = ref}
            onFocus={() => this.setState({ isInputFocused: true })}
            onChangeText={onChangeValue}
            onSubmitEditing={this.focusDisable}
            onBlur={this.focusDisable}
            value={text}
            style={{
              width: '100%',
              flex: 1,
              paddingLeft: PixelRatio.getPixelSizeForLayoutSize(13.92),
              paddingRight: PixelRatio.getPixelSizeForLayoutSize(40.92),
              fontSize: PixelRatio.getPixelSizeForLayoutSize(7.36),
              color: '#46264a',
              fontFamily: 'SFUIDisplay',
              fontWeight: 'bold',
              // borderWidth: 1,
              // borderColor: 'red',
            }}
          />

          {text.length > 0 &&
            <TouchableOpacity
              onPress={() => {}}
              style={{
                height: '100%',
                position: 'absolute',
                right: 0,
                top: 0,
                bottom: 0,
                paddingLeft: PixelRatio.getPixelSizeForLayoutSize(2.4),
                paddingRight: PixelRatio.getPixelSizeForLayoutSize(12.28),
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Image
                source={lupaImg}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(8.64),
                  height: PixelRatio.getPixelSizeForLayoutSize(11.68),
                }}
              />
            </TouchableOpacity>
          }

        </InputContainer>

        {this.renderFoundVariants()}

        <TouchableOpacity
          onPress={() => this.searcInputRef.focus()}
          style={{
            // borderWidth: 1,
            // borderColor: 'red',
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            marginRight: PixelRatio.getPixelSizeForLayoutSize(12.16),
            marginTop: PixelRatio.getPixelSizeForLayoutSize(7.68),
          }}
        >
          <AdvancedSearchLabel>
            {dict.get('SCHEDULE_SEARCH_ADVANCED')}
            <AdvancedSearchLabel style={{ fontWeight: 'bold' }}> {dict.get('SCHEDULE_SEARCH_INPUT_SEARCH')}</AdvancedSearchLabel>
          </AdvancedSearchLabel>
          <Image
            source={arrowImg}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(3.52),
              height: PixelRatio.getPixelSizeForLayoutSize(6.24),
              marginLeft: PixelRatio.getPixelSizeForLayoutSize(4.63),
            }}
          />
        </TouchableOpacity>

      </Wrapper>
    );
  }

  static propTypes = {
    placeholder: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  };
}
