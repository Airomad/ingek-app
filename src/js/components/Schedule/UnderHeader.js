import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

const containerHeight = PixelRatio.getPixelSizeForLayoutSize(18.4);
const Container = styled.View`
  width: 100%;
  height: ${containerHeight};
  margin-bottom: 20px;
`;

const LabelContainer = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  height: ${containerHeight};
  justify-content: center;
  align-items: flex-start;
  padding-left: ${PixelRatio.getPixelSizeForLayoutSize(13.92)};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(13.92)};
`;

const Label = styled.Text`
  font-family: SFUIText;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 500;
  text-align: center;
  color: #ffffff;
`;


export default class UnderHeader extends Component {
  render() {
    const { text } = this.props;

    return (
        <Container>
          <Svg
            width="100%"
            height={containerHeight}
            preserveAspectRatio="none"
            style={{ flex: 1 }}
          >
            <Defs>
              <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
                <Stop offset="0%" stopColor="#491a50" />
                <Stop offset="100%" stopColor="#e41884" />
              </LinearGradient>
            </Defs>
            
            <Rect
              width="100%"
              height="100%"
              fill="url(#pbbg)"
            />
          </Svg>

          <LabelContainer>
            <Label numberOfLines={1}>{text && text.toUpperCase()}</Label>
          </LabelContainer>
        </Container>
    );
  }

  static propTypes = {
    text: PropTypes.string.isRequired,
  };
}
