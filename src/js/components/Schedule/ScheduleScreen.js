import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import UnderHeader from 'components/Schedule/UnderHeader';
import FindButton from 'components/Schedule/FindButton';
import ScheduleFormModal from 'components/Schedule/ScheduleFormModal';

import teacherImg from 'icons/icon-teacher-grey.png';
import placeGreyImg from 'icons/icon-place-grey.png';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  background-color: #ffffff;
  flex: 1;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(48.16)};
  padding-bottom: ${PixelRatio.getPixelSizeForLayoutSize(13.6)};
`;

const RegularLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: 14px;
  color: #c6c6c6;
  margin-top: 100px;
  margin-left: 40px;
  margin-right: 40px;
  margin-bottom: 100px;
`;

const PlatesWrapper = styled.View`
  width: 100%;
  justify-content: flex-start;
  align-items: stretch;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
`;

const PlateContainer = styled.View`
  width: 100%;
  flex-direction: row;
`;

const TimeContainer = styled.View`
  width: ${PixelRatio.getPixelSizeForLayoutSize(44.64)};
`;

const TimeLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(9.6)};
  font-weight: 500;
  text-align: right;
  color: ${({ type }) => type === 'start' ? '#46264a' : '#c6c6c6'};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(2.24)};
`;

const LectureWrapper = styled.View`
  flex: 1;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(6.08)};
  margin-right: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
`;

const lecturePaddingSide = PixelRatio.getPixelSizeForLayoutSize(5.92);
const LectureContainer = styled.View`
  flex: 1;
  background-color: #f7f7f7;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(5.12)};
  padding-bottom: ${PixelRatio.getPixelSizeForLayoutSize(7.52)};
  padding-left: ${lecturePaddingSide};
  padding-right: ${lecturePaddingSide};
  border: ${PixelRatio.getPixelSizeForLayoutSize(0.32)}px solid #ececec;
`;

const LecutreLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.72)};
  font-weight: 500;
  text-align: left;
  color: #969696;
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(5.76)};
`;

const LectureName = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  font-weight: 500;
  text-align: left;
  color: #46264a;
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(6.4)};
`;

const LectureDataLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: 12px;
  text-align: left;
  color: #c6c6c6;
  margin-right: 15px;
`;



const plateSeparatorWidth = PixelRatio.getPixelSizeForLayoutSize(3.68);
function PlateSeparator() {
  return (
    <View
      style={{
        width: plateSeparatorWidth,
        alignItems: 'center',
      }}
    >
      <View 
        style={{ 
          width: plateSeparatorWidth,
          height: plateSeparatorWidth,
          borderRadius: PixelRatio.getPixelSizeForLayoutSize(1.92),
          backgroundColor: '#e6e6e6',
          borderWidth: PixelRatio.getPixelSizeForLayoutSize(0.32),
          borderColor: '#320f37',
        }}
      />
      <View style={{ flex: 1, width: PixelRatio.getPixelSizeForLayoutSize(0.8), backgroundColor: '#ededed' }} />
    </View>
  );
}

export default class ScheduleScreen extends Component {
  static propTypes = {
    
  };

  renderPlate = (data, key) => {
    const { date, time_start, time_end, name, type, teacher, audience, housing, withNavigation } = data;

    return (
      <PlateContainer key={`sh${key}`}>
        <TimeContainer>
          <TimeLabel type="start">{time_start}</TimeLabel>
          <TimeLabel type="end">{time_end}</TimeLabel>
        </TimeContainer>
        <PlateSeparator />
        <LectureWrapper>
          <LectureContainer>
            <LecutreLabel>{type}</LecutreLabel>
            <LectureName>{name}</LectureName>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: PixelRatio.getPixelSizeForLayoutSize(5.12),
              }}
            >
              <Image
                source={teacherImg}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(7.68),
                  height: PixelRatio.getPixelSizeForLayoutSize(7.04),
                  marginRight: PixelRatio.getPixelSizeForLayoutSize(4),
                }}
              />
              <LectureDataLabel numberOfLines={1}>{teacher}</LectureDataLabel>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginBottom: PixelRatio.getPixelSizeForLayoutSize(1.44),
              }}
            >
              <Image
                source={placeGreyImg}
                style={{
                  width: PixelRatio.getPixelSizeForLayoutSize(6.25),
                  height: PixelRatio.getPixelSizeForLayoutSize(8.29),
                  marginRight: PixelRatio.getPixelSizeForLayoutSize(4.8),
                }}
              />
              <LectureDataLabel>{dict.get('SCHEDULE_AUDIENCE')}: {audience} / {dict.get('SCHEDULE_BUILDING')}: {housing}</LectureDataLabel>
            </View>
          </LectureContainer>
        {withNavigation &&
          <FindButton
            text={dict.get('SCHEDULE_FIND_AUDIENCE')}
          />
        }
        </LectureWrapper>
      </PlateContainer>
    );
  }

  renderSchedule = () => {
    const { data } = this.props;
    console.log(data);
    if (data) {
      const rows = [];
      data.forEach((item, index) => {
        rows.push(this.renderPlate(item, index));
      });
      return (
        <PlatesWrapper>
          <UnderHeader
            text={data[0].date}
          />
          {rows}
        </PlatesWrapper>
      );
    } else return (
      <RegularLabel>{dict.get('SCHEDULE_NO_DATA')}</RegularLabel> 
    );
  }

  render() {
    const { navigation, alert, data, loading, requestsCache } = this.props;
    //const { email, password, signInMethod } = this.state;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          contentContainerStyle={{
            minHeight: 640 - STATUSBAR_HEIGHT,
            // height: Dimensions.get('window').height - STATUSBAR_HEIGHT,
            // borderWidth: 1,
            // borderColor: 'red',
          }}
        >
          <Wrapper>

            <HeaderNavigation
              type="default"
              theme="dark"
              title={dict.get('SCHEDULE_TITLE')}
              onGoBack={() => navigation.navigate('news')}
              onMenu={() => navigation.toggleDrawer({
                side: 'left',
                animated: true,
              })}
            />

            {loading &&
              <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <ActivityIndicator size="small" color="#320f37" />
              </View>
            }

            {!loading && this.renderSchedule() }

          </Wrapper>

          {alert && alert()}

        </ScrollView>

        <ScheduleFormModal
          data={requestsCache}
        />

      </View>
    );
  }
}
