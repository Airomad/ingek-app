import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import RoundedButton from 'components/RoundedButton';
import logoImg from 'images/logo-big-schedule.png';

export const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const PlateContainer = styled.View`
  border: 1px solid #E5E5E5;
  flex-direction: column;
  margin: 10px;
  padding: 10px;
`;

const PlateTitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: 17px;
  font-weight: 500;
  text-align: left;
  color: #320f37;
  margin-bottom: 15px;
`;

const PlateTextLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: 15px;
  font-weight: 300;
  text-align: left;
  color: #c6c6c6;
`;

class Test extends PureComponent {
  render() {
    return (
      <PlateContainer>
    </PlateContainer>
    )
  }
}

export default class FeedScreen extends Component {
  static propTypes = {
    
  };

  renderItem = ({ item }) => (
    <PlateContainer key={`feedp${item.id}`}>
      <PlateTitleLabel>{item.title}</PlateTitleLabel>
      <PlateTextLabel>{item.text}</PlateTextLabel>
    </PlateContainer>
  );

  renderHeader = () => {
    const { navigation } = this.props;
    return (
      <View style={{ height: 100 }}>
        <HeaderNavigation
          type="default"
          theme="dark"
          title={dict.get('NOTIFICATIONS_TITLE')}
          onGoBack={() => navigation.navigate('news')}
          onMenu={() => navigation.toggleDrawer({
            side: 'left',
            animated: true,
          })}
        />
      </View>
    );
  }

  renderFooter = () => {
    const { loading, isLoadedAllData } = this.props;

    if (!loading && !isLoadedAllData) {
      return (
        <View
          style={{
            flex: 1,
            height: 200,
            // borderWidth: 1,
            // borderColor: 'red',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 20,
          }}
        >
          <ActivityIndicator size="small" color="#320f37" />
        </View>
      );
    } else {
      return null;
    }
  }

  render() {
    const { navigation, loading, alert, data, onRefreshPage, onLoadMoreData, isLoadedAllData } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <View>
        <FlatList
          data={data}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          keyExtractor={(item, index) => `feedk${index}`}
          renderItem={this.renderItem}
          onRefresh={onRefreshPage}
          refreshing={loading}
          onEndReachedThreshold={0.1}
          onEndReached={!isLoadedAllData ? onLoadMoreData : null}
          initialNumToRender={8}
          maxToRenderPerBatch={2}
        />

        {alert && alert()}

      </View>
    );
  }
}
