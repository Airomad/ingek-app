import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import RoundedButton from 'components/RoundedButton';
import PickerButton from 'components/Settings/PickerButton';
import ToggleButton from 'components/Settings/ToggleButton';


export const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
  padding-top: ${PixelRatio.getPixelSizeForLayoutSize(44.96)};
`;

const Separator = styled.View`
  width: 100%;
  height: ${PixelRatio.getPixelSizeForLayoutSize(0.48)};
  background-color: #ededed;
`;

const marginSide = PixelRatio.getPixelSizeForLayoutSize(13.92);
const TitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(4.8)};
  font-weight: 500;
  text-align: left;
  color: #bcbcbc;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(12.64)};
  margin-left: ${marginSide};
  margin-right: ${marginSide};
`;

const ItemsWrapper = styled.View`
  margin-left: ${marginSide};
  margin-right: ${marginSide};
`;


export default class ScheduleLockedScreen extends Component {
  static propTypes = {
    
  };

  getLabelForLanguage = (langId) => {
    const { languages } = this.props;
    if (languages) {
      const filtered = languages.filter(item => item.value === langId);
      if (filtered && filtered[0]) {
        return filtered[0].label;
      }
    }
    return 'undefined';
  }

  render() {
    const {
      navigation,
      loading,
      languages,
      onToggleNotifications,
      onSetLanguage,
      data,
      totalNotificationsCount,
    } = this.props;

    let settingsData = data || {
      notificationsEnabled: false,
      language: 'en',
    };

    const {
      notificationsEnabled,
      language,
    } = settingsData;

    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <ScrollView
        contentContainerStyle={{
          minHeight: 640 - STATUSBAR_HEIGHT,
          //height: Dimensions.get('window').height - STATUSBAR_HEIGHT,
          // borderWidth: 1,
          // borderColor: 'red',
        }}
      >
        <Wrapper>

          <HeaderNavigation
            type="default"
            theme="dark"
            title={dict.get('SETTINGS_TITLE')}
            onGoBack={() => navigation.navigate('news')}
            onMenu={() => navigation.toggleDrawer({
              side: 'left',
              animated: true,
            })}
          />

        <Separator />

        {loading &&
          <View 
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center', 
            }}
          >
            <ActivityIndicator size="large" color="#320f37" />
          </View>
        }

        {!loading &&
          <View>
            <TitleLabel numberOfLines={1}>{dict.get('SETTINGS_SECTION_GENERAL_TITLE').toUpperCase()}</TitleLabel>
            <ItemsWrapper>
              <PickerButton
                type="select"
                label={dict.get('SETTINGS_ITEM_LANGUAGE')}
                value={this.getLabelForLanguage(language)}
                onChangeValue={onSetLanguage}
                items={languages}
              />
              <PickerButton
                onPress={() => navigation.navigate('profileEdit')}
                label={dict.get('SETTINGS_ITEM_EDITING_PROFILE')}
              />
            </ItemsWrapper>

            <Separator />

            <TitleLabel numberOfLines={1}>{dict.get('SETTINGS_SECTION_NOTIFICATIONS_TITLE').toUpperCase()}</TitleLabel>
            <ItemsWrapper>
              <ToggleButton
                onToggle={onToggleNotifications}
                label={dict.get('SETTINGS_ITEM_NOTIFICATIONS')}
                active={notificationsEnabled}
              />
              <PickerButton
                onPress={() => navigation.navigate('notifications')}
                label={dict.get('SETTINGS_ITEM_ALL_NOTIFICATIONS')}
                value={`${totalNotificationsCount}`}
                valueBold
              />
            </ItemsWrapper>

            <Separator />

            <TitleLabel numberOfLines={1}>{dict.get('SETTINGS_SECTION_SCHEDULE_TITLE').toUpperCase()}</TitleLabel>
            <ItemsWrapper>
              <PickerButton
                label={dict.get('SETTINGS_ITEM_SCHEDULE_LIST')}
              />
            </ItemsWrapper>
          </View>
        }

        </Wrapper>

      </ScrollView>
    );
  }
}
