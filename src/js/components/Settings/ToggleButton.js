import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';


const ValueLabel = styled.Text`
  max-width: ${PixelRatio.getPixelSizeForLayoutSize(48)};
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  color: #828282;
  font-weight: 300;
  text-align: right;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
`;

const Label = ValueLabel.extend`
  max-width: auto;
  font-weight: 500;
  text-align: left;
  color: #320f37;
  flex: 1;
  margin-left: 0;
`;


function Toggle({ active }) {
  const circleSide = PixelRatio.getPixelSizeForLayoutSize(9.68);
  const containerWidth = PixelRatio.getPixelSizeForLayoutSize(22.72);
  const circleOffset = PixelRatio.getPixelSizeForLayoutSize(1);

  return (
    <View
      style={{
        width: containerWidth,
        height: PixelRatio.getPixelSizeForLayoutSize(11.68),
        justifyContent: 'center',
        borderRadius: PixelRatio.getPixelSizeForLayoutSize(5.76),
        backgroundColor: active ? '#4c1043' : '#bababa',
      }}
    >
      <View
        style={{
          width: circleSide,
          height: circleSide,
          borderRadius: circleSide / 2,
          backgroundColor: '#ffffff',
          transform: [
            {translateX: active ? circleOffset : containerWidth - circleOffset - circleSide}
          ]
        }}
      />
    </View>
  );
}

export default class ToggleButton extends Component {
  static propTypes = {
    active: PropTypes.bool,
    onToggle: PropTypes.func,
  };

  static defaultProps = {
    active: false,
    onToggle: undefined,
  };

  // state = {
  //   isActive: false,
  // };

  render() {
    const { label, value, active, onToggle } = this.props;
    const marginTopBottom = PixelRatio.getPixelSizeForLayoutSize(3.2);

    return (
      <TouchableOpacity
        onPress={onToggle}
        activeOpacity={0.6}
        style={{
          height: PixelRatio.getPixelSizeForLayoutSize(13.6),
          //borderWidth: 1,
         // borderColor: 'red',
          flexDirection: 'row',
          justifyContent: 'flex-start',
          alignItems: 'center',
          marginTop: marginTopBottom,
          marginBottom: marginTopBottom,
        }}
      >
        <Label numberOfLines={1}>{label}</Label>
        {value &&
          <ValueLabel numberOfLines={1}>{value}</ValueLabel>
        }
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Toggle 
            active={active}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
