import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  View,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';

import RNPickerSelect from 'react-native-picker-select';

import arrowImg from 'icons/icon-input-select-big.png';


const ValueLabel = styled.Text`
  max-width: ${PixelRatio.getPixelSizeForLayoutSize(48)};
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  color: ${({ bold }) => bold ? '#320f37' : '#828282'};
  font-weight: 300;
  text-align: right;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
`;

const Label = ValueLabel.extend`
  max-width: auto;
  font-weight: 500;
  text-align: left;
  color: #320f37;
  flex: 1;
  margin-left: 0;
`;

export default class PickerButton extends Component {
  static propTypes = {
    type: PropTypes.oneOf(['button', 'select']),
    items: PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
    onChangeValue: PropTypes.func,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    type: 'button',
    items: [],
    onChangeValue: undefined,
    onPress: undefined,
  };

  render() {
    const { label, value, valueBold, type, items, onChangeValue, onPress } = this.props;
    const marginTopBottom = PixelRatio.getPixelSizeForLayoutSize(3.2);

    if (type === 'button') {
      return (
        <TouchableOpacity
          onPress={onPress ? onPress : () => {}}
          activeOpacity={0.6}
          style={{
            height: PixelRatio.getPixelSizeForLayoutSize(13.6),
            //borderWidth: 1,
           // borderColor: 'red',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: marginTopBottom,
            marginBottom: marginTopBottom,
          }}
        >
          <Label numberOfLines={1}>{label}</Label>
          {value &&
            <ValueLabel numberOfLines={1} bold={valueBold}>{value}</ValueLabel>
          }
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Image
              source={arrowImg}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(3.52),
                height: PixelRatio.getPixelSizeForLayoutSize(6.24),
                marginLeft: PixelRatio.getPixelSizeForLayoutSize(7.36),
              }}
            />
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <RNPickerSelect
        items={items}
        placeholder={{}}
        onValueChange={onChangeValue}
        style={{
          headlessAndroidContainer: { flex: 1 },
          height: PixelRatio.getPixelSizeForLayoutSize(13.6),
        }}
      >
        <View
          style={{
            // borderWidth: 1,
            // borderColor: 'red',
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: marginTopBottom,
            marginBottom: marginTopBottom,
          }}
        >
          <Label numberOfLines={1}>{label}</Label>
          {value &&
            <ValueLabel numberOfLines={1} bold={valueBold}>{value}</ValueLabel>
          }
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              // borderWidth: 1,
              // borderColor: 'blue',
            }}
          >
            <Image
              source={arrowImg}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(3.52),
                height: PixelRatio.getPixelSizeForLayoutSize(6.24),
                marginLeft: PixelRatio.getPixelSizeForLayoutSize(7.36),
              }}
            />
          </View>
        </View>
      </RNPickerSelect>
      );
    }
  }
}
