import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  View,
} from 'react-native';
import styled from 'styled-components/native';
import RNPickerSelect from 'react-native-picker-select';

import icon from 'icons/icon-input-select-big.png';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  height: ${PixelRatio.getPixelSizeForLayoutSize(16)};
  height: 20px;
`;

const Label = styled.Text`
  flex: 1;
  color: ${({ active }) => active ? '#320f37' : '#858585'};
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
`;

export default class InputSelectBig extends Component {
  state = {
    item: '',
  };

  onValueChange = (value) => {
    this.setState({ item: value });
    this.props.onValueChange && this.props.onValueChange(value);
  }

  render() {
    const {
      placeholder,
      withoutPaddingLeft,
      loading,
      items,
      value,
      disabled,
    } = this.props;
    const { item } = this.state;

    if (loading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size="small" color="#320f37" />
        </View>
      );
    } else {
      const itemsData = items && items.length > 0 ? items : [{ label: '', value: null }];
      return (
        <RNPickerSelect
          //items={[{label: 'one', value: 1}, {label: 'two', value: 2}]}
          //placeholder={{}}
          items={itemsData}
          onValueChange={this.onValueChange}
          style={{ headlessAndroidContainer: { flex: 1 }}}
          disabled={disabled}
         >
          <Wrapper>
            <Label active={value !== ''}>{value || placeholder}</Label>
            <Image
              source={icon}
              style={{
                width: PixelRatio.getPixelSizeForLayoutSize(3.52),
                height: PixelRatio.getPixelSizeForLayoutSize(6.24),
                marginLeft: PixelRatio.getPixelSizeForLayoutSize(5),
                marginRight: 0,
              }}
            />
          </Wrapper>
        </RNPickerSelect>
      );
    }
  }
}
