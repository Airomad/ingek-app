import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
} from 'react-native';
import styled from 'styled-components/native';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
  height: ${PixelRatio.getPixelSizeForLayoutSize(16)};
`;

const IconContainer = styled.View`
  height: 100%;
  width: ${PixelRatio.getPixelSizeForLayoutSize(11.24)};
  justify-content: center;
  align-items: center;
`;

export default class InputText extends Component {
  state = {
    text: '',
  };

  render() {
    const {
      placeholder,
      icon,
      secureTextEntry,
      onFocus,
      onEndEditing,
      withoutPaddingLeft,
      onChangeText,
      value,
      valid,
    } = this.props;
    const { text } = this.state;

    return (
      <Wrapper>
        {icon &&
          <IconContainer>
            <Image
              source={text.length > 0 ? icon.sourceTriggered : icon.source}
              style={{ width: icon.width, height: icon.height }}
            />
          </IconContainer>
        }
        <TextInput
          style={{
            height: '100%',
            width: '100%',
            color: valid ? '#320f37' : '#A10606',
            paddingLeft: withoutPaddingLeft ? 0 : PixelRatio.getPixelSizeForLayoutSize(7.04),
            fontSize: PixelRatio.getPixelSizeForLayoutSize(6.88),
          }}
          secureTextEntry={secureTextEntry}
          underlineColorAndroid="transparent"
          multiline={false}
          onChangeText={onChangeText}
          placeholder={placeholder}
          placeholderTextColor={valid ? "#989898" : "#A10606"}
          value={value}
          onFocus={onFocus}
          onEndEditing={onEndEditing}
        />
      </Wrapper>
    );
  }
}
