import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  Image,
  TouchableOpacity,
  Picker,
} from 'react-native';
import styled from 'styled-components/native';
import RNPickerSelect from 'react-native-picker-select';

import icon from 'icons/icon-input-select-small.png';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  height: ${PixelRatio.getPixelSizeForLayoutSize(16)};
  justify-content: ${({ align }) => align || 'flex-start'}
`;

const Label = styled.Text`
  color: ${({ active }) => active ? '#320f37' : '#858585'};
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
`;

export default class InputSelectSmall extends Component {
  state = {
    itemLabel: '',
  };

  selectItem = (item) => {
    const { items, onSelect } = this.props;

    const filteredItems = items.filter(entry => entry.value === item);
    const selectedItem = filteredItems[0] && filteredItems[0].label;
    this.setState({ itemLabel: selectedItem });
    onSelect && onSelect(item);
  }

  render() {
    const {
      placeholder,
      withoutPaddingLeft,
      align,
    } = this.props;
    const { itemLabel } = this.state;
    const { items } = this.props;

    return (
      <RNPickerSelect
        items={items}
        onValueChange={this.selectItem}
        style={{ headlessAndroidContainer: { flex: 1 }}}
      >
        <Wrapper align={align}>
          <Label active={itemLabel} numberOfLines={1}>{itemLabel || placeholder}</Label>
          <Image
            source={icon}
            style={{
              width: PixelRatio.getPixelSizeForLayoutSize(6.24),
              height: PixelRatio.getPixelSizeForLayoutSize(3.52),
              marginLeft: PixelRatio.getPixelSizeForLayoutSize(5),
              marginRight: 0,
            }}
          />
        </Wrapper>
      </RNPickerSelect>
    );
  }
}
