import React, { Component } from 'react';
import {
  TextInput,
  PixelRatio,
  TouchableOpacity,
  Image,
  View,
} from 'react-native';
import styled from 'styled-components/native';

import DateTimePicker from 'react-native-modal-datetime-picker';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
  height: ${PixelRatio.getPixelSizeForLayoutSize(16)};
`;

const IconContainer = styled.View`
  height: 100%;
  width: ${PixelRatio.getPixelSizeForLayoutSize(11.24)};
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  font-size: 14px;
  color: ${({ placeholder }) => placeholder ? 'grey' : '#320f37'};
  paddingLeft: ${({ withoutPaddingLeft }) => (withoutPaddingLeft ? 0 : PixelRatio.getPixelSizeForLayoutSize(7.04))};
`;

export default class InputText extends Component {
  state = {
    isDatePickerVisible: false,
  };

  showDatePicker = () => this.setState({ isDatePickerVisible: true });

  hideDatePicker = () => this.setState({ isDatePickerVisible: false });

  prepareDateString = (date) => {
    const dateArr = date.toString().split(' ');
    const monthStr = dateArr[1].toLowerCase();
    let month;
    if (monthStr === 'jan') {
      month = '01';
    }
    if (monthStr === 'feb') {
      month = '02';
    }
    if (monthStr === 'mar') {
      month = '03';
    }
    if (monthStr === 'apr') {
      month = '04';
    }
    if (monthStr === 'may') {
      month = '05';
    }
    if (monthStr === 'jun') {
      month = '06';
    }
    if (monthStr === 'jul') {
      month = '07';
    }
    if (monthStr === 'aug') {
      month = '08';
    }
    if (monthStr === 'sep') {
      month = '09';
    }
    if (monthStr === 'oct') {
      month = '10';
    }
    if (monthStr === 'nov') {
      month = '11';
    }
    if (monthStr === 'dec') {
      month = '12';
    }

    return `${dateArr[3]}-${month}-${dateArr[2]}`;
  }

  handleDatePicked = (date) => {
    const preparedDate = this.prepareDateString(date);
    this.hideDatePicker();

    const { onChangeText } = this.props;
    onChangeText && onChangeText(preparedDate);
  };

  render() {
    const {
      placeholder,
      secureTextEntry,
      onFocus,
      onEndEditing,
      withoutPaddingLeft,
      onChangeText,
      value,
      valid,
    } = this.props;
    const { isDatePickerVisible } = this.state;

    return (
      <Wrapper>
        <TouchableOpacity
          onPress={this.showDatePicker}
          style={{
            height: '100%',
            width: '100%',
            paddingLeft: withoutPaddingLeft ? 0 : PixelRatio.getPixelSizeForLayoutSize(7.04),
            justifyContent: 'center',
          }}
        >
          <Label withoutPaddingLeft={withoutPaddingLeft} placeholder={!value}>{value || placeholder}</Label>
        </TouchableOpacity>
        <DateTimePicker
          isVisible={isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDatePicker}
        />
      </Wrapper>
    );
  }
}
