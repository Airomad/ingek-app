import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  Keyboard,
  Animated,
  ScrollView,
  NativeModules,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import dict from 'utils/dictionary';

import logoImg from 'images/logo-big.png';
import emailIcon from 'icons/icon-email.png';
import emailIconTriggered from 'icons/icon-email-triggered.png';
import passwordIcon from 'icons/icon-password.png';
import passwordIconTriggered from 'icons/icon-password-triggered.png';
import googleIcon from 'icons/icon-google.png';
import fbIcon from 'icons/icon-fb.png';
import twitterIcon from 'icons/icon-twitter.png';

import InputText from 'components/LoginForm/InputText';
import RoundedButton from 'components/RoundedButton';


export const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const logoContainerHeight = PixelRatio.getPixelSizeForLayoutSize(90);
export const LogoContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex: 0.9;
`;

const formContainerMargin = PixelRatio.getPixelSizeForLayoutSize(14.08);
export const FormContainer = styled.View`
  flex: 1;
  margin-left: ${formContainerMargin};
  margin-right: ${formContainerMargin};
  flex: 1;
`;

export const LogoLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.5)};
  font-weight: bold;
  text-align: center;
  color: #a6a6a6;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(9.76)};
`;

export const SimpleLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
  color: #989898;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(5.44)};
`;

export const SocialButtonsContainer = styled.View`
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(2.08)};
  flex-direction: row;
  justify-content: center;
`;

export const FooterContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  height: ${PixelRatio.getPixelSizeForLayoutSize(19.04)};
`;

export const LinkLabel = SimpleLabel.extend`
  font-weight: 800;
  color: #320f37;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(2)};
`;

export default class SignInScreen extends Component {
  state = {
    signInMethod: 'default',
  };


  setSignInMethod = (signInMethod) => {
    this.setState({ signInMethod });
  }

  onSocialLogin = () => {
    const { onSignInFb, onSignInGoogle } = this.props;
    const { signInMethod } = this.state;

    ///console.log(this.state);

    switch (signInMethod) {
      case 'Facebook':
        onSignInFb();
        break;
      case 'Google':
        onSignInGoogle();
        break;
      default: return null;
    }
  }

  render() {
    const logoSize = PixelRatio.getPixelSizeForLayoutSize(54);
    const {
      login,
      password,
      onLoginChange,
      onPasswordChange,
      navigation,
      onLogin,
      loading,
      validationResult: { loginValid, passwordValid },
      alert,
    } = this.props;
    const { signInMethod } = this.state;
    const sociaIconMarginSide = PixelRatio.getPixelSizeForLayoutSize(3.4);
    const socialIconPadding = PixelRatio.getPixelSizeForLayoutSize(4);
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <ScrollView
        contentContainerStyle={{
          minHeight: 640 - STATUSBAR_HEIGHT,
          height: Dimensions.get('window').height - STATUSBAR_HEIGHT,
        }}
      >
        <Wrapper>
          <LogoContainer>
            <Image source={logoImg} style={{ width: logoSize, height: logoSize }} />
            <LogoLabel>{dict.get('SIGNIN_TITLE')}</LogoLabel>
          </LogoContainer>

          <FormContainer>
            <Row>
              <InputText
                placeholder={dict.get('SIGNIN_INPUT_EMAIL')}
                icon={{
                  source: emailIcon,
                  sourceTriggered: emailIconTriggered,
                  width: PixelRatio.getPixelSizeForLayoutSize(11.68),
                  height: PixelRatio.getPixelSizeForLayoutSize(7.52),
                }}
                onChangeText={onLoginChange}
                value={login}
                valid={loginValid}
              />
            </Row>
            <Row style={{ marginBottom: PixelRatio.getPixelSizeForLayoutSize(21.12)}}>
              <InputText
                placeholder={dict.get('SIGNIN_INPUT_PASSWORD')}
                icon={{
                  source: passwordIcon,
                  sourceTriggered: passwordIconTriggered,
                  width: PixelRatio.getPixelSizeForLayoutSize(10.24),
                  height: PixelRatio.getPixelSizeForLayoutSize(11.68),
                }}
                secureTextEntry
                onChangeText={onPasswordChange}
                value={password}
                valid={passwordValid}
              />
              <TouchableOpacity
                onPress={() => navigation.navigate('restorePassword')}
                activeOpacity={0.5}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 10,
                }}
              >
                <SimpleLabel>{dict.get('SIGNIN_FORGOT')}</SimpleLabel>
              </TouchableOpacity>
            </Row>
            <RoundedButton
              onPress={onLogin}
              text={dict.get('SIGNIN_MAIN_BUTTON')}
              loading={loading}
            />

            {signInMethod !== 'default' &&
              <RoundedButton
                onPress={this.onSocialLogin}
                text={dict.get('SIGNIN_WITH_BUTTON')}
                text2={signInMethod}
                buttonType="type3"
                style={{ marginTop: PixelRatio.getPixelSizeForLayoutSize(6.56) }}
                loading={loading}
              />
            }

            <SocialButtonsContainer>
              <TouchableOpacity
                onPress={() => this.setSignInMethod('Twitter')}
                style={{
                  padding: socialIconPadding,
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={twitterIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(11.2),
                    height: PixelRatio.getPixelSizeForLayoutSize(7.36),
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setSignInMethod('Google')}
                style={{
                  padding: socialIconPadding,
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={googleIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(11.2),
                    height: PixelRatio.getPixelSizeForLayoutSize(7.36),
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setSignInMethod('Facebook')}
                style={{
                  padding: socialIconPadding,
                  paddingTop: PixelRatio.getPixelSizeForLayoutSize(3),
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={fbIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(3.84),
                    height: PixelRatio.getPixelSizeForLayoutSize(8.48),
                  }}
                />
              </TouchableOpacity>
            </SocialButtonsContainer>
          </FormContainer>

          <FooterContainer>
            <SimpleLabel>{dict.get('SIGNIN_SIGNUP_LABEL')}</SimpleLabel>
            <TouchableOpacity
              onPress={() => navigation.navigate('signUp')}
              activeOpacity={0.5}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <LinkLabel>{dict.get('SIGNUP_TITLE')}</LinkLabel>
            </TouchableOpacity>
          </FooterContainer>

        </Wrapper>

        {alert && alert()}

      </ScrollView>
    );
  }
}
