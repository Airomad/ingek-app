import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  Keyboard,
  Animated,
  ScrollView,
  NativeModules,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';

import FBSDK from 'react-native-fbsdk';
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken
} = FBSDK;

import {
  Wrapper,
  LogoContainer,
  LogoLabel,
  FormContainer,
  Row,
  SimpleLabel,
  FooterContainer,
  LinkLabel,
  SocialButtonsContainer,
} from './SignInScreen';

import dict from 'utils/dictionary';

import logoImg from 'images/logo-big.png';
import emailIcon from 'icons/icon-email.png';
import emailIconTriggered from 'icons/icon-email-triggered.png';
import passwordIcon from 'icons/icon-password.png';
import passwordIconTriggered from 'icons/icon-password-triggered.png';
import googleIcon from 'icons/icon-google.png';
import fbIcon from 'icons/icon-fb.png';
import twitterIcon from 'icons/icon-twitter.png';

import InputText from 'components/LoginForm/InputText';
import RoundedButton from 'components/RoundedButton';
//import Alert from 'components/Alert';


export default class SignUpScreen extends Component {
  state = {
    isPasswordVisible: false,
    signUpMethod: 'default',
  };

  togglePasswordVisibility = () => {
    this.setState({ isPasswordVisible: !this.state.isPasswordVisible });
  }

  setSignUpMethod = (signUpMethod) => {
    this.setState({ signUpMethod });
  }

  render() {
    const logoSize = PixelRatio.getPixelSizeForLayoutSize(54);
    const {
      login,
      password,
      onLoginChange,
      onPasswordChange,
      navigation,
      onRegister,
      loading,
      validationResult: { loginValid, passwordValid },
      alert,
      onSignUpFb,
    } = this.props;
    const {
      isPasswordVisible,
      signUpMethod,
    } = this.state;
    const sociaIconMarginSide = PixelRatio.getPixelSizeForLayoutSize(3.4);
    const socialIconPadding = PixelRatio.getPixelSizeForLayoutSize(4);
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    
    return (
      <ScrollView
        contentContainerStyle={{
          minHeight: 640 - STATUSBAR_HEIGHT,
          height: Dimensions.get('window').height - STATUSBAR_HEIGHT,
          // borderWidth: 1,
          // borderColor: 'red',
        }}
      >
        <Wrapper>

          <LogoContainer>
            <Image source={logoImg} style={{ width: logoSize, height: logoSize }} />
            <LogoLabel>{dict.get('SIGNUP_TITLE')}</LogoLabel>
          </LogoContainer>

          <FormContainer>
            <Row>
              <InputText
                placeholder={dict.get('SIGNUP_INPUT_EMAIL')}
                icon={{
                  source: emailIcon,
                  sourceTriggered: emailIconTriggered,
                  width: PixelRatio.getPixelSizeForLayoutSize(11.68),
                  height: PixelRatio.getPixelSizeForLayoutSize(7.52),
                }}
                onChangeText={onLoginChange}
                value={login}
                valid={loginValid}
              />
            </Row>
            <Row style={{ marginBottom: PixelRatio.getPixelSizeForLayoutSize(21.12)}}>
              <InputText
                placeholder={dict.get('SIGNUP_INPUT_PASSWORD')}
                icon={{
                  source: passwordIcon,
                  sourceTriggered: passwordIconTriggered,
                  width: PixelRatio.getPixelSizeForLayoutSize(10.24),
                  height: PixelRatio.getPixelSizeForLayoutSize(11.68),
                }}
                secureTextEntry={!isPasswordVisible}
                onChangeText={onPasswordChange}
                value={password}
                valid={passwordValid}
              />
              <TouchableOpacity
                onPress={this.togglePasswordVisibility}
                activeOpacity={0.5}
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: 10,
                }}
              >
                <SimpleLabel>{isPasswordVisible ? dict.get('SIGNUP_HIDE') : dict.get('SIGNUP_SHOW')}</SimpleLabel>
              </TouchableOpacity>
            </Row>
            <RoundedButton
              onPress={onRegister}
              text={dict.get('SIGNUP_MAIN_BUTTON')}
              buttonType="type2"
              loading={loading}
            />
            
            {signUpMethod !== 'default' &&
              <RoundedButton
                onPress={onSignUpFb}
                text={dict.get('SIGNUP_WITH_BUTTON')}
                text2={signUpMethod}
                buttonType="type3"
                style={{ marginTop: PixelRatio.getPixelSizeForLayoutSize(6.56) }}
                loading={loading}
              />
            }

            <SocialButtonsContainer>
              <TouchableOpacity
                onPress={() => this.setSignUpMethod('Twitter')}
                style={{
                  padding: socialIconPadding,
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={twitterIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(11.2),
                    height: PixelRatio.getPixelSizeForLayoutSize(7.36),
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setSignUpMethod('Google')}
                style={{
                  padding: socialIconPadding,
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={googleIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(11.2),
                    height: PixelRatio.getPixelSizeForLayoutSize(7.36),
                  }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => this.setSignUpMethod('Facebook')}
                style={{
                  padding: socialIconPadding,
                  paddingTop: PixelRatio.getPixelSizeForLayoutSize(3),
                  marginLeft: sociaIconMarginSide,
                  marginRight: sociaIconMarginSide,
                }}
              >
                <Image
                  source={fbIcon}
                  style={{
                    width: PixelRatio.getPixelSizeForLayoutSize(3.84),
                    height: PixelRatio.getPixelSizeForLayoutSize(8.48),
                  }}
                />
              </TouchableOpacity>
            </SocialButtonsContainer>
          </FormContainer>

          <FooterContainer>
            <SimpleLabel>{dict.get('SIGNUP_SIGNIN_LABEL')}</SimpleLabel>
            <TouchableOpacity
              onPress={() => navigation.navigate('signIn')}
              activeOpacity={0.5}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <LinkLabel>{dict.get('SIGNIN_TITLE')}</LinkLabel>
            </TouchableOpacity>
          </FooterContainer>

        </Wrapper>

        {alert && alert()}

      </ScrollView>
    );
  }
}
