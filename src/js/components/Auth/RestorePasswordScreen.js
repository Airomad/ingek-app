import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  Image,
  PixelRatio,
  Keyboard,
  Animated,
  ScrollView,
  NativeModules,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import logoImg from 'images/logo-big.png';
import emailIcon from 'icons/icon-email.png';
import emailIconTriggered from 'icons/icon-email-triggered.png';

import InputText from 'components/LoginForm/InputText';
import RoundedButton from 'components/RoundedButton';


export const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
`;

const logoContainerHeight = PixelRatio.getPixelSizeForLayoutSize(90);
export const LogoContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex: 0.9;
`;

const formContainerMargin = PixelRatio.getPixelSizeForLayoutSize(14.08);
export const FormContainer = styled.View`
  flex: 1;
  margin-left: ${formContainerMargin};
  margin-right: ${formContainerMargin};
  flex: 1;
`;

export const LogoLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.5)};
  font-weight: bold;
  text-align: center;
  color: #a6a6a6;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(9.76)};
`;

export const SimpleLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.88)};
  color: #989898;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: stretch;
  margin-bottom: 30;
`;

export const SocialButtonsContainer = styled.View`
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(2.08)};
  flex-direction: row;
  justify-content: center;
`;

export const FooterContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  height: ${PixelRatio.getPixelSizeForLayoutSize(19.04)};
`;

export const LinkLabel = SimpleLabel.extend`
  font-weight: 800;
  color: #320f37;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(2)};
`;

const TextLabel = styled.Text`
  padding-left: 20px;
  padding-right: 20px;
  color: #989898;
  font-size: 16px;
`;

export default class SignInScreen extends Component {
  renderDefaultPanel = () => {
    const { onLoginChange, login, validationResult: { loginValid }, onRestorePassword, loading} = this.props;
    return (
      <FormContainer>
        <Row>
          <InputText
            placeholder="Your Email"
            icon={{
              source: emailIcon,
              sourceTriggered: emailIconTriggered,
              width: PixelRatio.getPixelSizeForLayoutSize(11.68),
              height: PixelRatio.getPixelSizeForLayoutSize(7.52),
            }}
            onChangeText={onLoginChange}
            value={login}
            valid={loginValid}
          />
        </Row>
        <RoundedButton
          onPress={onRestorePassword}
          text="RESTORE PASSWORD"
          loading={loading}
        />

      </FormContainer>
    );
  }

  renderSuccessPanel = () => {
    const { navigation } = this.props;
    return (
      <FormContainer>
        <Row>
          <TextLabel>Success! A new password was sent on your E-mail address!</TextLabel>
        </Row>
        <RoundedButton
          onPress={() => navigation.navigate('signIn')}
          text="BACK"
        />

      </FormContainer>
    );
  }

  render() {
    const logoSize = PixelRatio.getPixelSizeForLayoutSize(54);
    const {
      navigation,
      alert,
      operationSuccess,
    } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;

    return (
      <ScrollView
        contentContainerStyle={{
          minHeight: 640 - STATUSBAR_HEIGHT,
          height: Dimensions.get('window').height - STATUSBAR_HEIGHT,
        }}
      >
        <Wrapper>
          <LogoContainer>
            <Image source={logoImg} style={{ width: logoSize, height: logoSize }} />
            <LogoLabel>Restore Password</LogoLabel>
          </LogoContainer>

          {operationSuccess && this.renderSuccessPanel() }

          {!operationSuccess && this.renderDefaultPanel() }

          <FooterContainer>
            <TouchableOpacity
              onPress={() => navigation.navigate('signIn')}
              activeOpacity={0.5}
              style={{
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <LinkLabel>Sign In</LinkLabel>
            </TouchableOpacity>
          </FooterContainer>

        </Wrapper>

        {alert && alert()}

      </ScrollView>
    );
  }
}
