import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
} from 'react-native';
import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import dict from 'utils/dictionary';

import HeaderNavigation from 'components/HeaderNavigation';
import RoundedButton from 'components/RoundedButton';
import RadioItem from 'components/RadioItem';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: #ffffff;
  flex: 1;
  padding-left: ${PixelRatio.getPixelSizeForLayoutSize(14.24)};
  padding-right: ${PixelRatio.getPixelSizeForLayoutSize(14.24)};
`;

const TitleLabel = styled.Text`
  font-family: SFUIText;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(4.8)};
  font-weight: 600;
  text-align: left;
  color: #cccccc;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(72.2)};
`;

const ItemsContainer = styled.View`
  flex: 1;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(10)};
  margin-bottom: ${PixelRatio.getPixelSizeForLayoutSize(10)};
  justify-content: center;
`;

export const BackBtnLabel = styled.Text`
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(6.32)};
  text-align: center;
  color: #939393;
`;

export default class RegistrationScreenStep1 extends Component {
  render() {
    const { navigation, nextStep, studentType, onSetStudentType, onMoveBack, onMoveNext, alert } = this.props;
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    const labelStyle = {
      marginLeft: PixelRatio.getPixelSizeForLayoutSize(37.12),
    };
    const radioItemStyle = {
      marginBottom: PixelRatio.getPixelSizeForLayoutSize(16),
    };
    
    return (
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{
          minHeight: Dimensions.get('window').height - STATUSBAR_HEIGHT,//640 - STATUSBAR_HEIGHT - Header.HEIGHT,
          //height: Dimensions.get('window').height - STATUSBAR_HEIGHT - Header.HEIGHT,
          //borderWidth: 1,
          //borderColor: 'green',
        }}
      >
        <Wrapper>

          <HeaderNavigation
            type="next-operation"
            theme="dark"
            title={dict.get('REGISTER_TITLE').toUpperCase()}
            applyDisabled={studentType === ''}
            onGoBack={onMoveBack}
            onApply={onMoveNext}
          />

          <TitleLabel>{dict.get('REGISTER_CHOOSE_TYPE').toUpperCase()}</TitleLabel>

          <ItemsContainer>
            <RadioItem
              text={dict.get('USER_TYPE_STUDENT')}
              active={studentType === 'student'}
              style={radioItemStyle}
              labelStyle={labelStyle}
              onPress={() => onSetStudentType('student')}
            />
            <RadioItem
              text={dict.get('USER_TYPE_FOREIGN')}
              active={studentType === 'f_student'}
              style={radioItemStyle}
              labelStyle={labelStyle}
              onPress={() => onSetStudentType('f_student')}
            />
            <RadioItem
              text={dict.get('USER_TYPE_TEACHER')}
              active={studentType === 'teacher'}
              style={radioItemStyle}
              labelStyle={labelStyle}
              onPress={() => onSetStudentType('teacher')}
            />
            <RadioItem
              text={dict.get('USER_TYPE_ENROLEE')}
              active={studentType === 'enrolee'}
              labelStyle={labelStyle}
              onPress={() => onSetStudentType('enrolee')}
            />
          </ItemsContainer>

          <RoundedButton
            onPress={onMoveNext}
            text={dict.get('REGISTER_NEXT_STEP').toUpperCase()}
            disabled={studentType === ''}
            style={{  }}
          />
          <TouchableOpacity
            onPress={onMoveBack}
            style={{
              paddingTop: PixelRatio.getPixelSizeForLayoutSize(5),
              paddingBottom: PixelRatio.getPixelSizeForLayoutSize(5),
              marginTop: PixelRatio.getPixelSizeForLayoutSize(9.72),
              marginBottom: PixelRatio.getPixelSizeForLayoutSize(8.12),
            }}
          >
            <BackBtnLabel>{dict.get('BACK_FOOTER_BUTTON_LABEL')}</BackBtnLabel>
          </TouchableOpacity>
        </Wrapper>

        {alert && alert() }
        
      </ScrollView>
    );
  }
}
