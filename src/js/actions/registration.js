import { REG_SET_STUDENT_TYPE, REG_SET_SEX } from 'actions/types';

export function setStudentType(studentType) {
  return {
    type: REG_SET_STUDENT_TYPE,
    studentType,
  }
}

export function setSex(sex) {
  return {
    type: REG_SET_SEX,
    sex,
  }
}
