import {
  API_SET_AUTH_DATA,
  API_SET_SETTINGS,
  LOGIN_INCORECT_DATA,
  INCORECT_INPUT_DATA,
  LOGIN_REQUIRED,
  LOADING_START,
  SERVER_RETURNED_BAD_JSON,
  SERVER_INTERNAL_ERROR,
} from 'actions/types';

import {
  INCORRECT_JSON_RESPONSE_FROM_SERVER
} from 'actions/errors_code';

import { clearUserLoginData } from 'utils';

export const BASE_URL = 'https://www.site3.hneu.edu.ua/app';


export function request({
  url,
  method = 'GET',
  query = {},
  data = undefined,
}, handler) {
  return async (dispatch, getState) => {
    dispatch({ type: LOADING_START });
    
    const { token } = getState();
    
    let result;
    
    if (token) {
      query.token = token;
    }
    const qs =
      Object
        .entries(query)
        .filter(it => Boolean(it[1]))
        .map(it => `${encodeURIComponent(it[0])}=${encodeURIComponent(it[1])}`)
        .join('&');

    console.log(`Doing request for: ${BASE_URL}/${url}?${qs}`);
    
    const req = await fetch(`${BASE_URL}/${url}?${qs}`, {
      method,
      body: data && JSON.stringify(data),
      timeout: 10000,
    }).catch(error => console.log(error));

    result = await req.json().catch(error => console.log(error));

    console.log('Result is: ', result);

    if (result && result.code) {
      switch (result.code) {
        case 1: 
          return handler(
            {
              data: result.data,
            }, 
            dispatch,
            getState
          );
        case 2:
          dispatch({
            type: INCORECT_INPUT_DATA,
            token: null,
            lastErrors: result.errors || result.form_errors,
          });
          break;
        case 3:
          const lastErrors = (result.errors || result.form_errors) || [990];
          console.log(lastErrors);
          dispatch({
            type: SERVER_INTERNAL_ERROR,
            lastErrors,
          });
          break;
        case 4:
          dispatch({
            type: INCORECT_INPUT_DATA,
            token: null,
            lastErrors: result.errors || result.form_errors,
          });
          break;
        case 6:
          clearUserLoginData(); //test

          dispatch({
            type: LOGIN_REQUIRED,
            token: null,
            lastErrors: [1001],
          });
          break;
        default: return null;
      }
    } else {
      dispatch({
        type: SERVER_RETURNED_BAD_JSON,
        lastErrors: INCORRECT_JSON_RESPONSE_FROM_SERVER,
      });
    }
  };
}

export function validateAuthToken(token, login, password) {
  return request(  
    {
      url: 'check_token',
      query: {
        login, token
      }
    },
    ({ data }, dispatch) => {
      console.log('User token is valid!');
      console.log(data);
      dispatch({
        type: API_SET_AUTH_DATA,
        token,
        login,
        password,
      });
    },
  );
}

export function setAuthData(token, login, password) {
  return {
    type: API_SET_AUTH_DATA,
    token,
    login,
    password,
  }
}

export function setSettings(settingsData) {
  return {
    type: API_SET_SETTINGS,
    settingsData,
  }
}
