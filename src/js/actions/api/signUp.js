import { request } from '../utils';
import {
  API_SET_AUTH_DATA,
  SIGN_UP_SET_FILLING_INFO,
  LOADING_END,
} from 'actions/types';

export function signUp(login, password) {
  return request(
    {
      url: 'registration',
      query: { login, pass: password },
    },
    ({ data }, dispatch) => {
      // console.log(data);
      dispatch({
        type: API_SET_AUTH_DATA,
        token: data.token,
        login,
      });
    },
  );
}

export function restorePassword(login, handler) {
  return request(
    {
      url: 'reset_pass',
      query: { login },
    },
    ({ data }, dispatch) => {
      // console.log(data);
      dispatch({
        type: LOADING_END,
      });
      handler && handler(login);
    },
  );
}

export function registerFillData({ token, user_type, fname, lname, faculty, group, photo, sex, date_born, code_ver, country, patronymic }) {
  return request(
    {
      url: 'filling_data',
      query: { token, user_type, fname, lname, faculty, group, photo, sex, date_born, code_ver, country, patronymic },
    },
    ({ data }, dispatch) => {
      // console.log(data);
      dispatch({
        type: SIGN_UP_SET_FILLING_INFO,
        //token: data.token,
      });
    },
  );
}
