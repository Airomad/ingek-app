import { request } from '../utils';
import { API_SET_SEARCH_AUTOCOMPLETE_DATA } from 'actions/types';


export function loadAutoComplete(token) {
  return request(
    {
      url: 'get_short_news',
      query: { token },
    },
    ({ data }, dispatch) => {
      console.log(data);

      const autoCompleteData = [
        { label: 'j jfskdjfj', value: 123 },
        { label: 'jsdkfjks d', value: 345 }
      ];

      dispatch({
        type: API_SET_SEARCH_AUTOCOMPLETE_DATA,
        data: autoCompleteData,
      });
    },
  );
}
