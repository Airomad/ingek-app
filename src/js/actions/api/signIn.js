import { request, setAuthData } from '../utils';
import { API_SET_AUTH_DATA } from 'actions/types';

export function signIn(login, password) {
  return request(
    {
      url: 'login',
      query: { login, pass: password },
    },
    ({ data }, dispatch) => {
      console.log(data);
      dispatch(setAuthData(data.token, login));
    },
  );
}
