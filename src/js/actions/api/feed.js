import { request } from '../utils';
import {
  API_SET_FEED_INIT_DATA,
  API_SET_FEED_EXTRA_DATA,
  API_SET_FEED_TOTAL_COUNT,
} from 'actions/types';


export function loadFeedData(isInitial, offset = 0, size = 20) {
  return request(
    {
      url: 'get_notifications',
      query: { offset, size },
    },
    ({ data }, dispatch) => {
      console.log(data);
      dispatch({
        type: isInitial ? API_SET_FEED_INIT_DATA : API_SET_FEED_EXTRA_DATA,
        data,
      });
    },
  );
}

export function getFeedTotalCount(token) {
  return request(
    {
      url: 'get_notifications_total_count',
      query: { token },
    },
    ({ data }, dispatch) => {
      console.log(data);
      dispatch({
        type: API_SET_FEED_TOTAL_COUNT,
        totalCount: data.count,
      });
    },
  );
}
