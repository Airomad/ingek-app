import { request, setAuthData } from '../utils';
import {
  API_SET_USER_DATA,
  API_SET_AUTH_DATA,
  API_SET_USER_TEMP_PHOTO,
  API_SET_USER_PHOTO_HASH,
  API_USER_DATA_UPDATE_START,
  API_SET_USER_FACULTIES,
  API_SET_USER_GROUPS,
} from 'actions/types';

export function socialLogin(socialType, accessToken) {
  return request(
    {
      url: 'login_soc',
      query: {
        soc_type: socialType,
        access_token: accessToken,
      },
    },
    ({ data }, dispatch) => {
      console.log('Auth with social ', socialType);
      console.log(data);
      dispatch(setAuthData(data.token, data.login));
    },
  );
}

export function loadUserData(token) {
  return request(
    {
      url: 'get_user',
      query: { token },
    },
    ({ data }, dispatch) => {
      console.log(data);
      dispatch({
        type: API_SET_USER_DATA,
        data,
      });
    },
  );
}

export function logout(token, login) {
  return request(
    {
      url: 'logout',
      query: { token, login },
    },
    ({ data }, dispatch) => {
      dispatch({
        type: API_SET_AUTH_DATA,
        token: null,
        login: null,
      });
    },
  );
}
 
export function updateUserInfo(token, userData, photoHash) {
  if (!userData) {
    console.error('Empty user data! Can\'nt update user info!');
  }
  console.log('User data is', userData);

  const { fname, lname, type, password, sex, date_born, faculty, group } = userData;
  const query = {
    token,
    fname,
    lname,
    type,
    pass: password,
    sex,
    date_born,
    faculty,
    group
  };
  if (photoHash) {
    query.photo = photoHash;
  }

  return request(
    {
      url: 'edit_user',
      query,
    },
    ({ data }, dispatch) => {
      console.log('Updated user data');
      console.log(data);
      dispatch({
        type: API_SET_USER_DATA,
        data: userData,
      });
    },
  );
}

export function updateUserTempPhoto(source, token) {

  return async (dispatch, getState) => {

    dispatch({
      type: API_SET_USER_TEMP_PHOTO,
    });

    var data = new FormData();  
    data.append('user_pict', {  
      uri: source.uri, // your file path string
      name: 'filename.jpg',
      type: 'image/jpg'
    });

    await fetch('https://www.site3.hneu.edu.ua/app/upload_photo', {  
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'multipart/form-data'
      },
      method: 'POST',
      body: data
    })
      .then((response) => response.json())
      .then((responseJson) => {
          switch (responseJson.code) {
            case 1: 
              if (responseJson.status === 'ok') {
                const { photo_hash } = responseJson.data;
                dispatch({
                  type: API_SET_USER_PHOTO_HASH,
                  photoHash: photo_hash,
                });
                
              } else {
                dispatch({
                  type: API_SET_USER_PHOTO_HASH,
                  photoHash: null,
                });
                console.error('Cant\'nt update user photo!!');
                
              }
              break;
              
            default:
              dispatch({
                type: API_SET_USER_PHOTO_HASH,
                photoHash: null,
              });
             
              break;
          }

      }).catch((error) => {
          console.error(error);
      });
  }
}

export function getFaculties() {
  return request(
    {
      url: 'get_faculties',
    },
    ({ data }, dispatch) => {
      dispatch({
        type: API_SET_USER_FACULTIES,
        faculties: data,
      });
    },
  );
}

export function getGroups() {
  return request(
    {
      url: 'get_groups',
    },
    ({ data }, dispatch) => {
      dispatch({
        type: API_SET_USER_GROUPS,
        groups: data,
      });
    },
  );
}
