import { request } from '../utils';
import {
  API_SET_NEWS_INIT_DATA,
  API_SET_NEWS_EXTRA_DATA,
  API_SET_NEWS_VIEW,
} from 'actions/types';

// TODO THIS IS GOVNOCODE
const months = [
  {id: '01', value: 'JAN'},
  {id: '02', value: 'FEB'},
  {id: '03', value: 'MAR'},
  {id: '04', value: 'APR'},
  {id: '05', value: 'MAY'},
  {id: '06', value: 'JUN'},
  {id: '07', value: 'JUL'},
  {id: '08', value: 'AUG'},
  {id: '09', value: 'SEP'},
  {id: '10', value: 'OCT'},
  {id: '11', value: 'NOV'},
  {id: '12', value: 'DEC'},
];

export function loadNewsData(token, isInitial, offset = 0, size = 20) {
  return request(
    {
      url: 'get_short_news',
      query: { token, offset, size },
    },
    ({ data }, dispatch) => {
      console.log(data);
      const news = [];
      data.forEach((item, index) => {
        const content = {
          type: item.type,
          id: item.id,
          poster: item.image,
          text: {
            title: item.title,
            lines: [ item.text ],
          },
        };

        if (item.type === 'news') {
          const date = item.date.split('-');
          const seekingForMonth = months.filter(item => item.id === date[1].trim());
          const month = seekingForMonth && seekingForMonth[0] && seekingForMonth[0].value ? seekingForMonth[0].value : 'JAN';
          content.date = {
            day: date[2],
            month: month,
            year: date[0],
          };
        } else if (item.type === 'adv') {
          content.link = item.link;
        }

        news.unshift(content);
      });

      console.log(news);
      dispatch({
        type: isInitial ? API_SET_NEWS_INIT_DATA : API_SET_NEWS_EXTRA_DATA,
        data: news,
      });
    },
  );
}

export function loadNewsView(newsId) {
  return request(
    {
      url: 'get_news',
      query: { id: newsId },
    },
    ({ data }, dispatch) => {
      const newsData = {
        title: data[1],
        text: [ data.text ],
        id: data[0],
        images: data.images.filter(item => item),
        date: '12 JUN 2018',
      };
      dispatch({
        type: API_SET_NEWS_VIEW,
        data: newsData,
      });
    },
  );
}