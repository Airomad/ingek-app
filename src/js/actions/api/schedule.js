import { request } from '../utils';
import {
  API_SET_SCHEDULE_DATA,
} from 'actions/types';

export function loadScheduleDataDay(token, date) {
  return request(
    {
      url: 'get_shedule_day',
      query: { token, date },
    },
    ({ data }, dispatch) => {
      console.log(data);
      dispatch({
        type: API_SET_SCHEDULE_DATA,
        data,
      });
    },
  );
}
