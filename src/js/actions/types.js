export const REG_SET_STUDENT_TYPE = 'REG_SET_STUDENT_TYPE';
export const REG_SET_SEX = 'REG_SET_SEX';

export const CHECK_AUTH_TOKEN = 'CHECK_AUTH_TOKEN';
export const API_SET_AUTH_DATA = 'API_SET_AUTH_DATA';
export const INCORECT_INPUT_DATA = 'INCORECT_INPUT_DATA';
export const LOGIN_INCORECT_DATA = 'LOGIN_INCORECT_DATA';
export const LOGIN_REQUIRED = 'LOGIN_REQUIRED';

export const LOADING_START = 'LOADING_START';
export const LOADING_END = 'LOADING_END';

export const API_SET_USER_FACULTIES = 'API_SET_USER_FACULTIES';
export const API_SET_USER_GROUPS = 'API_SET_USER_GROUPS';
export const API_SET_NEWS_VIEW = 'API_SET_NEWS_VIEW';
export const API_SET_USER_DATA = 'API_SET_USER_DATA';
export const API_SET_USER_TEMP_PHOTO = 'API_SET_USER_TEMP_PHOTO';
export const API_SET_USER_PHOTO_HASH = 'API_SET_USER_PHOTO_HASH';
export const API_USER_DATA_UPDATE_START = 'API_USER_DATA_UPDATE_START';
export const API_SET_SEARCH_AUTOCOMPLETE_DATA = 'API_SET_SEARCH_AUTOCOMPLETE_DATA';
export const SIGN_UP_SET_FILLING_INFO = 'SIGN_UP_SET_FILLING_INFO';
export const API_SET_SETTINGS = 'API_SET_SETTINGS';
export const API_SET_FEED_INIT_DATA = 'API_SET_FEED_INIT_DATA';
export const API_SET_FEED_EXTRA_DATA = 'API_SET_FEED_EXTRA_DATA';
export const API_SET_FEED_TOTAL_COUNT = 'API_SET_FEED_TOTAL_COUNT';
export const API_SET_NEWS_INIT_DATA = 'API_SET_NEWS_INIT_DATA';
export const API_SET_NEWS_EXTRA_DATA = 'API_SET_NEWS_EXTRA_DATA';
export const API_SET_SCHEDULE_DATA = 'API_SET_SCHEDULE_DATA';

export const SERVER_RETURNED_BAD_JSON = 'SERVER_RETURNED_BAD_JSON';
export const SERVER_INTERNAL_ERROR = 'SERVER_INTERNAL_ERROR';