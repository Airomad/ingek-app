import { createStackNavigator, createSwitchNavigator, DrawerNavigator } from 'react-navigation';
import { ScrollView, Text, Dimensions } from 'react-native';

import FeedScreenContainer from 'containers/FeedScreenContainer';
import NewsScreenContainer from 'containers/News/NewsScreenContainer';
import NewsViewScreenContainer from 'containers/News/NewsViewScreenContainer';
import SideMenuContainer from 'containers/SideMenuContainer';
import ProfileScreenContainer from 'containers/ProfileScreenContainer';
import ProfileEditScreenContainer from 'containers/ProfileEditScreenContainer';
import SettingsScreenContainer from 'containers/SettingsScreenContainer';
import ScheduleScreenContainer from 'containers/Schedule/ScheduleScreenContainer';
import ScheduleSearchScreenContainer from 'containers/Schedule/ScheduleSearchScreenContainer';
import ScheduleLockedScreenContainer from 'containers/Schedule/ScheduleLockedScreenContainer';
import LogoutScreenContainer from 'containers/LogoutScreenContainer';
import NavigatorScreenContainer from 'containers/NavigatorScreenContainer';


const ProfileStack = createStackNavigator(
  {
    profile: ProfileScreenContainer,
    profileEdit: ProfileEditScreenContainer,
  },
  {
    initialRouteName: 'profile',
  }
);

const NewsStack = createStackNavigator(
  {
    news: NewsScreenContainer,
    newsView: NewsViewScreenContainer,
  },
  {
    initialRouteName: 'news',
  }
);

const AppStack = createSwitchNavigator(
  {
    feed: FeedScreenContainer,
    settings: SettingsScreenContainer,
    schedule: ScheduleScreenContainer,
    scheduleLocked: ScheduleLockedScreenContainer,
    scheduleSearch: ScheduleSearchScreenContainer,
    navigator: NavigatorScreenContainer,
    profile: ProfileStack,
    news: NewsStack,
  },
  {
    initialRouteName: 'feed',
  }
);

export default DrawerNavigator(
  {
    app: AppStack,
    logout: LogoutScreenContainer,
  },
  {
    contentComponent: SideMenuContainer,
    drawerWidth: Dimensions.get('window').width,
  }
);
