import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import SignInScreenContainer from 'containers/Auth/SignInScreenContainer';
import SignUpScreenContainer from 'containers/Auth/SignUpScreenContainer';
import RestorePasswordScreenContainer from 'containers/Auth/RestorePasswordScreenContainer';
import RegistrationScreenStep1Container from 'containers/Auth/RegistrationScreenStep1Container';
import RegistrationScreenStep2Container from 'containers/Auth/RegistrationScreenStep2Container';

const RegisterSetInfoStack = createStackNavigator(
  {
    registerStep1: RegistrationScreenStep1Container,
    registerStep2: RegistrationScreenStep2Container,
  },
  {
    initialRouteName: 'registerStep1',
  }
);

export default createSwitchNavigator(
  {
    signIn: SignInScreenContainer,
    signUp: SignUpScreenContainer,
    restorePassword: RestorePasswordScreenContainer,
    registerSetInfo: RegisterSetInfoStack,
  },
  {
    initialRouteName: 'signIn',
  },
);
