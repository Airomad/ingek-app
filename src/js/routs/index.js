import { createSwitchNavigator } from 'react-navigation';

import SplashScreenContainer from 'containers/SplashScreenContainer';
import AppRouts from './AppRouts';
import AuthRouts from './AuthRouts';

export default createSwitchNavigator(
  {
    splash: SplashScreenContainer,
    auth: AuthRouts,
    app: AppRouts,
  },
  {
    initialRouteName: 'splash',
  }
);
