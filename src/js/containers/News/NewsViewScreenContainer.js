import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import { loadNewsView } from 'actions/api/news';
import store from 'store';

import NewsViewScreen from 'components/News/NewsViewScreen';


export default class NewsViewScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    isLoading: false,
    data: null,
  };

  componentDidMount() {
    const newsId = this.props.navigation.getParam('id', 'NO-ID');
    store.dispatch(loadNewsView(newsId));
    
    let currentStore = store.getState();
    let isLoading = currentStore.app.loading;
    let data = currentStore.news.newsViewData;

    this.setState({
      isLoading,
      data,
    });

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      isLoading = currentStore.app.loading;
      data = currentStore.news.newsViewData;

      if (data && data !== this.state.data) {
        this.setState({
          isLoading,
          data
        });
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  render() {
    const { navigation } = this.props;
    const { isLoading, data } = this.state;

    return (
      <NewsViewScreen
        key="content"
        navigation={navigation}
        data={data}
        loading={isLoading}
      />
    );
  }
}
