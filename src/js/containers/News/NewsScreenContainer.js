import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';
import { Header } from 'react-navigation';

import { loadNewsData } from 'actions/api/news';
import store from 'store';

import NewsScreen from 'components/News/NewsScreen';


export default class NewsScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    loading: false,
    data: null,
    token: null,
    loadedAllData: false,
  };

  componentDidMount() {
    let currentStore = store.getState();
    let loading = currentStore.app.loading;
    let token = currentStore.app.token;
    let data = currentStore.news && currentStore.news.data;
    let loadedAllData = currentStore.news && currentStore.news.loadedAllData;

    store.dispatch(loadNewsData(token, true));

    let stateToUpdate = {};

    if (loading !== this.state.loading) {
      stateToUpdate.loading = loading;
    }
    if (data !== this.state.data) {
      stateToUpdate.data = data;
    }
    if (token !== this.state.token) {
      stateToUpdate.token = token;
    }
    if (loadedAllData !== this.state.loadedAllData) {
      stateToUpdate.loadedAllData = loadedAllData;
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      loading = currentStore.app.loading;
      token = currentStore.app.token;
      data = currentStore.news && currentStore.news.data;
      loadedAllData = currentStore.news && currentStore.news.loadedAllData;

      stateToUpdate = {};

      if (loading !== this.state.loading) {
        stateToUpdate.loading = loading;
      }
      if (data !== this.state.data) {
        stateToUpdate.data = data;
      }
      if (token !== this.state.token) {
        stateToUpdate.token = token;
      }
      if (loadedAllData !== this.state.loadedAllData) {
        stateToUpdate.loadedAllData = loadedAllData;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  refreshPage = () => {
    const { token } = this.state;

    if (token) {
      store.dispatch(loadNewsData(token, true));
    }
  }

  loadMoreData = (bottomScrollOffset) => {
    console.log('reched', bottomScrollOffset);
    const { token, data, loading } = this.state;
    if (!loading && token) {
      const currentOffset = (data && data.length) || 0;
      store.dispatch(loadNewsData(token, false, currentOffset));
    }
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }
  
  render() {
    const { navigation } = this.props;
    const { loading, data, loadedAllData } = this.state;

    return (
      <NewsScreen
        key="content"
        navigation={navigation}
        onRefreshPage={this.refreshPage}
        onLoadMoreData={this.loadMoreData}
        data={data}
        loading={loading}
        isLoadedAllData={loadedAllData}
        alert={this.alert}
      />
    );
  }
}
