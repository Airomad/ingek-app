import React, { Component } from 'react';
import {
  TouchableOpacity,
  Dimensions,
  PixelRatio,
  ScrollView,
  NativeModules,
  Platform,
  Image,
  View,
} from 'react-native';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';
import { Header } from 'react-navigation';

import dict from 'utils/dictionary';

import store from 'store';
import { loadUserData } from 'actions/api/user';
import { setUserLoginData } from 'utils';
import { getFaculties, getGroups } from 'actions/api/user';

const Wrapper = styled.View`
  width: 100%;
  flex-direction: column;
  justify-content: flex-start;
  align-items: stretch;
  background-color: transparent;
  flex: 1;
`;

const ContentWrapper = styled.View`
  position: absolute;
  left: ${PixelRatio.getPixelSizeForLayoutSize(10.56)};
  right: ${PixelRatio.getPixelSizeForLayoutSize(10.56)};
  top: ${PixelRatio.getPixelSizeForLayoutSize(45.6)};
  bottom: 0;
`;

const ContentContainer = styled.View`
  background: #ffffff;
  border-radius: ${PixelRatio.getPixelSizeForLayoutSize(4.8)};
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
  top: ${PixelRatio.getPixelSizeForLayoutSize(14.08)};
`;

const PhotoWrapper = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  align-items: center;
`;

const PhotoContainer = styled.View`
  width: ${PixelRatio.getPixelSizeForLayoutSize(53.44)};
  height: ${PixelRatio.getPixelSizeForLayoutSize(53.44)};
  border-radius: ${PixelRatio.getPixelSizeForLayoutSize(27.22)};
  background: #ffffff;
  justify-content: center;
  align-items: center;
`;

const NameLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(9.6)};
  font-weight: 500;
  text-align: center;
  color: #320f37;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(42.08)};
`;

const StudentTypeLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  text-align: center;
  color: #bdbdbd;
  margin-top: ${PixelRatio.getPixelSizeForLayoutSize(6.4)};
`;

const ListContainer = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ListItemLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.36)};
  font-weight: 600;
  text-align: center;
  color: #46264a;
`;


export default class SideMenuContainer extends Component {
  state = {
    userData: null,
  };

  componentDidMount() {
    store.dispatch(getFaculties());
    store.dispatch(getGroups());
    
    let currentStore = store.getState();
    let token = currentStore.app && currentStore.app.token;
    let userData = currentStore.user && currentStore.user.userData;

    if (token) {
      store.dispatch(loadUserData(token));
    }
    if (userData && userData !== this.state.userData) {
      this.setState({ userData });
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      userData = currentStore.user && currentStore.user.userData;

      if (userData && userData !== this.state.userData) {
        this.setState({ userData });

        //setUserLoginData(token, userData.email, true);
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  render() {
    const { navigation, nextStep, studentType, setStudentType } = this.props;
    const { userData } = this.state;
    const { fname, lname, photo, type } = userData ? userData : { fname: '...', lname: '...', photo: null, type: 'not detected' };
    const { StatusBarManager } = NativeModules;
    const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBarManager.HEIGHT;
    const height = Dimensions.get('window').height - STATUSBAR_HEIGHT;
    const listItemStyle = {
      width: '100%',
      height: PixelRatio.getPixelSizeForLayoutSize(8.64),
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: PixelRatio.getPixelSizeForLayoutSize(8),
      marginBottom: PixelRatio.getPixelSizeForLayoutSize(8),
    };
    const studentTypeLabels = {
      student: dict.get('USER_TYPE_STUDENT'),
      f_student: dict.get('USER_TYPE_FOREIGN'),
      teacher: dict.get('USER_TYPE_TEACHER'),
      enrolle: dict.get('USER_TYPE_ENROLEE'),
    };

    return (
      <View style={{ flex: 1 }}>
        <Svg
          width={Dimensions.get('window').width}
          height={height}
          style={{
            flex: 1,
          }}
        >
          <Defs>
            <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
              <Stop offset="0%" stopColor="#491a50" />
              <Stop offset="100%" stopColor="#e41884" />
            </LinearGradient>
          </Defs>
          <Rect
            width="100%"
            height="100%"
            fill="url(#pbbg)"
          />
        </Svg>
        <ScrollView
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            backgroundColor: 'transparent',
          }}
          contentContainerStyle={{
            minHeight: 640 - STATUSBAR_HEIGHT - Header.HEIGHT,
            height,
            backgroundColor: 'transparent',
          }}
        >
          <Wrapper>
            <ContentWrapper>
              <ContentContainer>
                <NameLabel>{fname} {lname}</NameLabel>
                <StudentTypeLabel>{studentTypeLabels[type]}</StudentTypeLabel>
                <ListContainer>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('news')}
                    style={listItemStyle}
                  >
                    <ListItemLabel>{dict.get('NEWS_TITLE').toUpperCase()}</ListItemLabel>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('scheduleLocked')}
                    style={listItemStyle}
                  >
                    <ListItemLabel>{dict.get('SCHEDULE_TITLE').toUpperCase()}</ListItemLabel>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('navigator')}
                    style={listItemStyle}
                  >
                    <ListItemLabel>{dict.get('NAVIGATOR_TITLE').toUpperCase()}</ListItemLabel>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('profile')}
                    style={listItemStyle}
                  >
                    <ListItemLabel>{dict.get('PROFILE_TITLE').toUpperCase()}</ListItemLabel>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => navigation.navigate('settings')}
                    style={listItemStyle}
                  >
                    <ListItemLabel>{dict.get('SETTINGS_TITLE').toUpperCase()}</ListItemLabel>
                  </TouchableOpacity>
                </ListContainer>
              </ContentContainer>

              <PhotoWrapper>
                <PhotoContainer>
                  <Image
                    source={{ uri: photo ? photo : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg' }}
                    style={{ width: PixelRatio.getPixelSizeForLayoutSize(42.24), height: PixelRatio.getPixelSizeForLayoutSize(42.24), borderRadius: PixelRatio.getPixelSizeForLayoutSize(27.22), }}
                  />
                </PhotoContainer>
              </PhotoWrapper>
            </ContentWrapper>
          </Wrapper>
        </ScrollView>
      </View>
    );
  }
}
