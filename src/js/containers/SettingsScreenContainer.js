import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import dict from 'utils/dictionary';

import store from 'store';
import { saveSettings } from 'utils';
import { getFeedTotalCount } from 'actions/api/feed';
import { setSettings } from 'actions/utils';

import SettingsScreen from 'components/Settings/SettingsScreen';

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  color: black;
  font-size: 20px;
`;

const languages = [
  { label: 'English', value: 'en' },
  { label: 'Русский', value: 'ru' },
  { label: 'Українська', value: 'uk' },
];

export default class SettingsScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    token: null,
    totalNotificationsCount: 0,
    settingsData: null,
    loading: false,
  };

  componentDidMount() {
    let currentStore = store.getState();
    let token = currentStore.app && currentStore.app.token;
    let settingsData = currentStore.app && currentStore.app.settingsData;
    let totalNotificationsCount = currentStore.feed && currentStore.feed.totalCount;
    let loading = currentStore.app && currentStore.app.loading;

    store.dispatch(getFeedTotalCount(token));

    let stateToUpdate = {};

    if (settingsData !== this.state.settingsData) {
      stateToUpdate.settingsData = settingsData;
    }
    if (token !== this.state.token) {
      stateToUpdate.token = token;
    }
    if (totalNotificationsCount !== this.state.totalNotificationsCount) {
      stateToUpdate.totalNotificationsCount = totalNotificationsCount;
    }
    if (loading !== this.state.loading) {
      stateToUpdate.loading = loading;
    }
    
    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      token = currentStore.app && currentStore.app.token;
      settingsData = currentStore.app && currentStore.app.settingsData;
      totalNotificationsCount = currentStore.feed && currentStore.feed.totalCount;
      loading = currentStore.app && currentStore.app.loading;

      stateToUpdate = {};
      
      if (settingsData !== this.state.settingsData) {
        stateToUpdate.settingsData = settingsData;
      }
      if (token !== this.state.token) {
        stateToUpdate.token = token;
      }
      if (totalNotificationsCount !== this.state.totalNotificationsCount) {
        stateToUpdate.totalNotificationsCount = totalNotificationsCount;
      }
      if (loading !== this.state.loading) {
        stateToUpdate.loading = loading;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  updateSettings = (settingsData) => {
    saveSettings(settingsData);
    store.dispatch(setSettings(settingsData));
  }

  toggleNotification = () => {
    const { settingsData } = this.state;
    if (settingsData) {
      const updatedSettingsData = {
        ...settingsData,
        notificationsEnabled: !settingsData.notificationsEnabled,
      }
      this.updateSettings(updatedSettingsData);
      //this.setState({ settingsData: updatedSettingsData });
    }
  }

  setLanguage = (langId) => {
    //console.log(langId);
    const { settingsData } = this.state;
    if (settingsData) {
      const updatedSettingsData = {
        ...settingsData,
        language: langId,
      }
      this.updateSettings(updatedSettingsData);
      dict.changeLanguage(langId);
      this.forceUpdate();
    }
  }

  render() {
    const { navigation } = this.props;
    const { settingsData, totalNotificationsCount, loading } = this.state;

    return (
      <SettingsScreen
        navigation={navigation}
        data={settingsData}
        totalNotificationsCount={totalNotificationsCount}
        onToggleNotifications={this.toggleNotification}
        onSetLanguage={this.setLanguage}
        languages={languages}
        loading={loading}
      />
    );
  }
}
