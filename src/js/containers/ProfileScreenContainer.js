import React, { Component } from 'react';

import store from 'store';
import dict from 'utils/dictionary';

import ProfileScreen from 'components/Profile/ProfileScreen';

export default class ProfileScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    userData: null,
  };

  prepareUserData = (userData, currentStore) => {
    const { faculty, group, sex } = userData;
    let facultyLabel = faculty;
    let groupLabel = group;
    let sexLabel = sex;

    const faculties = currentStore.user.faculties;
    const filteredFaculties = faculties.filter(item => item.faculty_id === faculty);
    if (filteredFaculties && filteredFaculties[0]) {
      facultyLabel = filteredFaculties[0].faculty_name;
    }

    const groups = currentStore.user.groups;
    const filteredGroups = groups.filter(item => item.group_id === group);
    if (filteredGroups && filteredGroups[0]) {
      groupLabel = filteredGroups[0].group_name;
    }

    if (sex === 'male') {
      sexLabel = dict.get('SEX_MALE');
    }
    if (sex === 'female') {
      sexLabel = dict.get('SEX_FEMALE');
    }

    return {
      ...userData,
      group: groupLabel,
      faculty: facultyLabel,
      sex: sexLabel,
    };
  }

  componentDidMount() {
    let currentStore = store.getState();
    let userData = currentStore.user && currentStore.user.userData;

    if (userData && userData !== this.state.userData) {
      this.setState({ userData: this.prepareUserData(userData, currentStore)});
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      userData = currentStore.user && currentStore.user.userData;

      if (userData && userData !== this.state.userData) {
        this.setState({ userData: this.prepareUserData(userData, currentStore)});
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  render() {
    const { navigation } = this.props;
    const { userData } = this.state;

    // const data = {
    //   image: userData && userData.photo ? userData.photo : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg',
    //   name: userData && userData.fname ? userData.fname : '',
    //   surname: userData && userData.lname ? userData.lname : '',
    //   role: userData && userData.type ? userData.type : 'not detected',
    //   email: userData && userData.login ? userData.login : '',
    //   password: 'something',
    //   sex: userData && userData.sex ? userData.sex : 'not detected',
    //   age: userData && userData.age ? userData.age : 'not detected',
    //   faculty: userData && userData.faculty ? userData.faculty : 'not detected',
    //   group: userData && userData.group ? userData.group : 'not detected',
    // };

    //console.log(data);

    return (
      <ProfileScreen
        navigation={navigation}
        data={userData}
      />
    );
  }
}
