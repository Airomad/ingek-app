import React, { Component } from 'react';
import { ActivityIndicator, View } from 'react-native';

import store from 'store';
import { logout } from 'actions/api/user';
import { clearUserLoginData } from 'utils';


export default class LogoutScreenContainer extends Component {
  componentDidMount() {
    let currentStore = store.getState();
    let token = currentStore.app && currentStore.app.token;
    const login = currentStore.user && currentStore.user.userData && currentStore.user.userData.login;

    store.dispatch(logout(token, login));

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      token = currentStore.app && currentStore.app.token;

      if (!token) {
        clearUserLoginData();
        this.props.navigation.navigate('splash');
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="small" color="#320f37" />
      </View>
    );
  }
}
