import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import ScheduleLockedScreen from 'components/Schedule/ScheduleLockedScreen';
import Alert from 'components/Alert';

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  color: black;
  font-size: 20px;
`;

export default class ScheduleLockedScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    errorMessage: 'Schedule service is offline!',
  };

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  render() {
    const { navigation } = this.props;

    return (
      <ScheduleLockedScreen
        navigation={navigation}
        loading={false}
        alert={this.alert}
      />
    );
  }
}
