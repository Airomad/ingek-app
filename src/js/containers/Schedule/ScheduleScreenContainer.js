import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import store from 'store';

import { loadScheduleDataDay } from 'actions/api/schedule';
import { getScheduleRequestsCache } from 'utils';

import ScheduleScreen from 'components/Schedule/ScheduleScreen';
import Alert from 'components/Alert';

const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  color: black;
  font-size: 20px;
`;

export default class ScheduleScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    errorMessage: 'Schedule service is offline!',
    token: null,
    data: null,
    loading: false,
    requestsCache: null,
  };

  componentDidMount() {
    const requestsCache = getScheduleRequestsCache();

    let currentStore = store.getState();
    let token = currentStore.app && currentStore.app.token;
    let loading = currentStore.app && currentStore.app.loading;
    let data = currentStore.schedule && currentStore.schedule.data;

    store.dispatch(loadScheduleDataDay('29068f30975f4c808f402f0a31e34fbf', '2018-07-28'));

    let stateToUpdate = {};

    if (token !== this.state.token) {
      stateToUpdate.token = token;
    }
    if (data !== this.state.data) {
      stateToUpdate.data = data;
    }
    if (loading !== this.state.loading) {
      stateToUpdate.loading = loading;
    }
    if (requestsCache !== this.state.requestsCache) {
      stateToUpdate.requestsCache = requestsCache;
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      token = currentStore.app && currentStore.app.token;
      loading = currentStore.app && currentStore.app.loading;
      data = currentStore.schedule && currentStore.schedule.data;

      if (token !== this.state.token) {
        stateToUpdate.token = token;
      }
      if (data !== this.state.data) {
        stateToUpdate.data = data;
      }
      if (loading !== this.state.loading) {
        stateToUpdate.loading = loading;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  render() {
    // const data = [
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //     withNavigation: true,
    //   },
    //   {
    //     time: {
    //       start: '07:30',
    //       end: '08:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    //   {
    //     time: {
    //       start: '08:30',
    //       end: '10:05'
    //     },
    //     lecture: 'Foreign language (in professional orientation)',
    //     teacher: 'Litvin Oleksandr Oleksandrovich',
    //     address: '215 (building 1)',
    //   },
    // ];
    const { navigation } = this.props;
    const { data, loading, requestsCache } = this.state;

    return (
      <ScheduleScreen
        navigation={navigation}
        loading={loading}
        data={data}
        requestsCache={requestsCache}
        alert={this.alert}
      />
    );
  }
}
