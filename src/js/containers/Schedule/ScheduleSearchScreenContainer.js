import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import store from 'store';
import { loadAutoComplete } from 'actions/api/search';

import ScheduleSearchScreen from 'components/Schedule/ScheduleSearchScreen';
import Alert from 'components/Alert';


export default class ScheduleScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  activeLoadingRequests = 0;

  state = {
    errorMessage: 'Schedule service is offline!',
    isLoading: false,
    searchInputText: '',
    autoCompleteData: null,
  };

  componentDidMount() {
    let currentStore = store.getState();
    let isLoading = currentStore.app.loading;
    let autoCompleteData = currentStore.search && currentStore.search.autoCompleteData;

    let stateToUpdate = {
      isLoading,
    };

    if (autoCompleteData && autoCompleteData !== this.state.autoCompleteData) {
      stateToUpdate.autoCompleteData = autoCompleteData;
      this.activeLoadingRequests -= 1;
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      isLoading = currentStore.app.loading;
      autoCompleteData = currentStore.search && currentStore.search.autoCompleteData;

      stateToUpdate = {
        isLoading,
      };

      if (autoCompleteData && autoCompleteData !== this.state.autoCompleteData) {
        stateToUpdate.autoCompleteData = autoCompleteData;
        this.activeLoadingRequests -= 1;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  // autoCompleteSearch = (text) => {
  //   const { searchInputText } = this.state;
  //   store.dispatch(loadAutoComplete(searchInputText));
  // }

  changeSearchInputText = (newText) => {
    this.setState({ searchInputText: newText });
    if (newText) {
      store.dispatch(loadAutoComplete(newText));
      this.activeLoadingRequests += 1;
    }
  }

  selectAutoCompleteVariant = (variant) => {
    this.setState({
      searchInputText: variant.label,
    });
  }

  render() {
    const { navigation } = this.props;
    const { isLoading, searchInputText, autoCompleteData } = this.state;
    
    console.log(this.activeLoadingRequests);

    return (
      <ScheduleSearchScreen
        navigation={navigation}
        loading={isLoading || this.activeLoadingRequests}
        searchInputText={searchInputText}
        autoCompleteData={autoCompleteData}
        onChangeSearchInputText={this.changeSearchInputText}
        onSelectAutoCompleteVariant={this.selectAutoCompleteVariant}
        alert={this.alert}
      />
    );
  }
}
