import React, { Component } from 'react';
import { AsyncStorage, Image, Dimensions, Animated } from 'react-native';
import styled from 'styled-components/native';

import Svg, {
  Defs,
  LinearGradient,
  Stop,
  Rect,
} from 'react-native-svg';

import dict from 'utils/dictionary';
import store from 'store';
import { signIn } from 'actions/api/signIn';
import { validateAuthToken, setSettings } from 'actions/utils';
import { getUserLoginData, clearUserLoginData, getSettings } from 'utils';

import logoImg from 'images/logo-big.png';
//import { settings } from 'cluster';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const LogoContainer = styled.View`
  width: 100px;
  height: 100px;
`;

const LoadingLineContainer = styled.View`
  background-color: #d7d7d7;
  height: 2px;
  position: absolute;
  bottom: 100px;
  left: 20%;
  right: 20%;
`;

function ProgressBarContent() {
  return (
    <Svg width="100%" height={2} preserveAspectRatio="none">
      <Defs>
        <LinearGradient id="pbbg" x1="0%" y1="0%" x2="100%" y2="0%">
          <Stop offset="0%" stopColor="#4b1a51" />
          <Stop offset="100%" stopColor="#e11883" />
        </LinearGradient>
      </Defs>
      <Rect
        width="100%"
        height="100%"
        fill="url(#pbbg)"
      />
    </Svg>
  );
}

export default class AuthScreenContainer extends Component {
  state = {
    loadingBarScale: new Animated.Value(0),
    loadingBarOffset: new Animated.Value(-(Dimensions.get('window').width * 0.3)),
  }

  componentDidMount() {
    const { loadingBarScale, loadingBarOffset } = this.state;
    Animated.parallel([
      Animated.timing(
        loadingBarScale,
        {
          toValue: 1,
          duration: 700,
          useNativeDriver: true,
        }
      ),
      Animated.timing(
        loadingBarOffset,
        {
          toValue: 0,
          duration: 700,
          useNativeDriver: true,
        }
      )
    ]).start();
    this.checkUserToken();
  }

  checkUserToken = async () => {
    const { navigation } = this.props;
    const userLoginData = await getUserLoginData();
    const settingsData = await getSettings();

    console.log('Settings data', settingsData);
    console.log('User login data', userLoginData);

    if (settingsData && settingsData.language) {
      dict.changeLanguage(settingsData.language);
    }

    if (userLoginData && settingsData) {

      const { login, token, password } = userLoginData;

      this.unsubscribeRedux = store.subscribe(() => {
        const currentStore = store.getState();

        if (currentStore.app && currentStore.app.token && currentStore.app.settingsData) {
          this.unsubscribeRedux && this.unsubscribeRedux();
          this.props.navigation.navigate('app');
        } else {
          this.unsubscribeRedux && this.unsubscribeRedux();
          this.props.navigation.navigate('auth');
        }
      });

      store.dispatch(validateAuthToken(token, login, password));
      store.dispatch(setSettings(settingsData));
    } else {
      this.props.navigation.navigate('auth');
    }
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  render() {
    const { loadingBarScale, loadingBarOffset } = this.state;
    const logoSize = Dimensions.get('window').width / 3;
    return (
      <Wrapper>
        <Image source={logoImg} style={{ width: logoSize, height: logoSize }} />
        <LoadingLineContainer>
          <Animated.View
            style={{
              transform: [
                { scaleX: loadingBarScale },
              ],
              flex: 1,
            }}
          >
            <ProgressBarContent />
          </Animated.View>
        </LoadingLineContainer>
      </Wrapper>
    );
  }
}
