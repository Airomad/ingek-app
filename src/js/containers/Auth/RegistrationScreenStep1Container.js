import React, { Component } from 'react';
import { TouchableOpacity, PixelRatio } from 'react-native';

import styled from 'styled-components/native';
import store from 'store';
import { setStudentType } from 'actions/registration';

import RegistrationScreenStep1 from 'components/Auth/RegistrationScreenStep1';
import Alert from 'components/Alert';


export const HeaderTitleLabel = styled.Text`
  font-family: SFUIDisplay;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.6)};
  font-weight: 500;
  text-align: center;
  color: #959595;
  margin-left: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
`;

export const HeaderBtnLabel = styled.Text`
  font-family: SFUIText;
  font-size: ${PixelRatio.getPixelSizeForLayoutSize(7.68)};
  font-weight: 500;
  text-align: right;
  color: ${({ disabled }) => disabled ? '#bcbcbc' : '#320f37'};
  margin-right: ${PixelRatio.getPixelSizeForLayoutSize(12.16)};
`;

export default class RegistrationScreenStep1Container extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    studentType: '',
    email: this.props.navigation.getParam('email', null),
    password: this.props.navigation.getParam('password', null),
    errorMessage: null,
  };

  setStudentType = (studentType) => {
    this.setState({ studentType });
  }

  onMoveBack = () => {
    const { navigation } = this.props;
    const currentState = store.getState();
    const token = store.app && store.app.token;

    if (token) {
      navigation.navigate('app');
    } else {
      navigation.navigate('signUp');
    }
  }

  onMoveNext = () => {
    const { studentType, email, password, errorMessage } = this.state;
    const { navigation } = this.props;

    if (['student', 'teacher', 'f_student', 'enrolee'].includes(studentType)) {
      navigation.navigate('registerStep2', {
        email,
        password,
        studentType,
      });
    } else {
      this.setState({ errorMessage: 'Incorrect user type. Please select one' });
    }
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  render() {
    const { navigation } = this.props;
    const { studentType } = this.state;

    return (
      <RegistrationScreenStep1
        navigation={navigation}
        studentType={studentType}
        onSetStudentType={this.setStudentType}
        onMoveBack={this.onMoveBack}
        onMoveNext={this.onMoveNext}
        alert={this.alert}
      />
    );
  }
}
