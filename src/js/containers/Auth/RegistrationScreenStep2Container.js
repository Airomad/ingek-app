import React, { Component } from 'react';
import { TouchableOpacity, PixelRatio } from 'react-native';

import styled from 'styled-components/native';
import store from 'store';

import {
  INCORRECT_INPUT_SEX,
  INCORRECT_INPUT_NAME,
  INCORRECT_INPUT_SURNAME,
  INCORRECT_INPUT_FACULTY,
  INCORRECT_INPUT_GROUP,
  INCORRECT_INPUT_USER_TYPE,
  INCORRECT_INPUT_VERIFICATION_CODE,
  INCORRECT_INPUT_COUNTRY,
  INCORRECT_INPUT_PATRONYMIC,
} from 'actions/errors_code';

import { setSex } from 'actions/registration';
import { getCoutriesList } from 'utils/countries';
import { getLastErrorMessageByCodes, isErrorCodeHandled } from 'utils/errorsHelper';
import { registerFillData } from 'actions/api/signUp';
import { getFaculties, getGroups } from 'actions/api/user';

import RegistrationScreenStep2 from 'components/Auth/RegistrationScreenStep2';
import Alert from 'components/Alert';

import { HeaderTitleLabel, HeaderBtnLabel } from './RegistrationScreenStep1Container';
import { getCountryLabelByCode } from '../../utils/countries';

export default class RegistrationScreenStep2Container extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    sex: '',
    faculty: null,
    group: null,
    facultyId: null,
    groupId: null,
    name: '',
    surname: '',
    date: '',
    photo: null,
    faculties: null,
    groups: null,
    filteredGroups: null,
    errorMessage: false,
    isLoading: false,
    isNameValid: true,
    isSurnameValid: true,
    verificationCode: null,
    isVerificationCodeValid: true,
    country: null,
    isCountryValid: true,
    countries: getCoutriesList(),
    patronymic: null,
    isPatronymicValid: true,
  };

  componentDidMount() {
    store.dispatch(getFaculties());
    store.dispatch(getGroups());

    let currentState = store.getState();
    let token = currentState.app && currentState.app.token;
    let isLoading = currentState.app.loading;
    let formErrors = currentState.app.lastErrors;
    let sex = currentState.registration.sex;
    let faculties = currentState.user && currentState.user.faculties;
    let groups = currentState.user && currentState.user.groups;
    let isRegistered = currentState.registration && currentState.registration.registered;
    let errorMessage = getLastErrorMessageByCodes(formErrors);
    let isNameValid = !isErrorCodeHandled(formErrors, INCORRECT_INPUT_NAME);

    let stateToUpdate = {
      sex,
      isLoading,
    };

    if (groups !== this.state.groups) {
      // const g = [];
      // groups.map((item, index) => {
      //   const newItem = {
      //     label: item.group_name,
      //     value: item.group_id,
      //   }
      //   g.push(newItem);
      // });
      stateToUpdate.groups = groups;
    }
    if (faculties !== this.state.faculties) {
      // const f = [];
      // faculties.map((item, index) => {
      //   const newItem = {
      //     label: item.faculty_id,
      //     value: item.faculty_name,
      //   }
      //   f.push(newItem);
      // })
      stateToUpdate.faculties = faculties;
    }

    if (errorMessage !== this.state.errorMessage) {
      stateToUpdate.errorMessage = errorMessage;
    }
    if (isNameValid !== this.state.isNameValid) {
      stateToUpdate.isNameValid = isNameValid;
    }

    this.setState(stateToUpdate);

    if (isRegistered && token) {
      this.props.navigation.navigate('app');
    }

    this.unsubscribeRedux = store.subscribe(
      () => {
        currentState = store.getState();
        token = currentState.app && currentState.app.token;
        isLoading = currentState.app.loading;
        formErrors = currentState.app && currentState.app.lastErrors;
        sex = currentState.registration.sex;
        groups = currentState.user && currentState.user.groups;
        faculties = currentState.user && currentState.user.faculties;
        isRegistered = currentState.registration && currentState.registration.registered;
        errorMessage = getLastErrorMessageByCodes(formErrors);
        isNameValid = !isErrorCodeHandled(formErrors, INCORRECT_INPUT_NAME);

        let stateToUpdate = {
          isLoading,
        };

        if (sex !== this.state.sex) {
          stateToUpdate.sex = sex;
        }
        if (groups !== this.state.groups) {
          // const g = [];
          // groups.map((item, index) => {
          //   const newItem = {
          //     label: item.group_name,
          //     value: item.group_id,
          //   }
          //   g.push(newItem);
          // });
          stateToUpdate.groups = groups;
        }
        if (faculties !== this.state.faculties) {
          // const f = [];
          // faculties.map((item, index) => {
          //   const newItem = {
          //     label: item.faculty_name,
          //     value: item.faculty_id,
          //   }
          //   f.push(newItem);
          // })
          stateToUpdate.faculties = faculties;
        }
        if (isNameValid !== this.state.isNameValid) {
          stateToUpdate.isNameValid = isNameValid;
        }
        if (errorMessage !== this.state.errorMessage) {
          stateToUpdate.errorMessage = errorMessage;
        }

        this.setState(stateToUpdate);

        if (isRegistered && token) {
          this.unsubscribeRedux && this.unsubscribeRedux();
          this.props.navigation.navigate('app');
        }
      }
    );
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  setSex = (sex) => {
    store.dispatch(setSex(sex));
  }

  setDate = (date) => this.setState({ date });
  setGroup = (group) => {
    if (this.state.groups && this.state.groups.length > 0) {
      const f = this.state.groups.filter(item => item.group_id === group);
      if (f && f[0]) {
        this.setState({ group: f[0].group_name, groupId: group });
      }
    }
  };
  setFaculty = (facultyId) => {
    const stateToUpdate = {};
    if (this.state.faculties && this.state.faculties.length > 0) {
      const f = this.state.faculties.filter(item => item.faculty_id === facultyId);
      if (f && f[0]) {
        stateToUpdate.faculty = f[0].faculty_name;
        stateToUpdate.facultyId = facultyId
      }
    }
    if (this.state.groups && this.state.groups.length > 0) {
      const g = this.state.groups.filter(item => item.faculty_id === facultyId);
      stateToUpdate.filteredGroups = g;

      if (g) {
        const fg = g.filter(item => item.group_id === this.state.groupId);
        if (!fg || fg.length < 1) {
          stateToUpdate.group = null;
          stateToUpdate.groupId = null;
        }
      }
    }
    this.setState(stateToUpdate);
  };
  setName = (name) => this.setState({ name });
  setSurname = (surname) => this.setState({ surname });
  setVerificationCode = (verificationCode) => this.setState({ verificationCode });
  setCountry = (country) => this.setState({ country });
  setPatronymic = (patronymic) => this.setState({ patronymic });

  onDone = () => {
    const currentState = store.getState();
    const token = currentState.app && currentState.app.token;

    const data = {
      token,
      sex: this.state.sex,
      date_born: this.state.date,
      fname: this.state.name,
      lname: this.state.surname,
      faculty: this.state.facultyId,
      group: this.state.groupId,
      user_type: this.props.navigation.getParam('studentType', ''),
      photo: this.state.photo,
      code_ver: this.state.verificationCode,
      country: this.state.country,
      patronymic: this.state.patronymic,
    };  
    store.dispatch(registerFillData(data));
  }

  render() {
    const { navigation } = this.props;
    const {
      sex,
      group,
      faculty,
      groups,
      filteredGroups,
      faculties,
      name,
      surname,
      date,
      isLoading,
      isNameValid,
      isSurnameValid,
      verificationCode,
      isVerificationCodeValid,
      country,
      isCountryValid,
      countries,
      patronymic,
      isPatronymicValid,
    } = this.state;
    const countryLabel = getCountryLabelByCode(country);
    
    const facultiesItems = [];
    if (faculties) {
      faculties.map((item, index) => {
        const newItem = {
          label: item.faculty_name,
          value: item.faculty_id,
        }
        facultiesItems.push(newItem);
      });
    }
    console.log('faculties', facultiesItems);

    const groupsItems = [];
    if (filteredGroups) {
      filteredGroups.map((item, index) => {
        const newItem = {
          label: item.group_name,
          value: item.group_id,
        }
        groupsItems.push(newItem);
      });
    }
    console.log('groups', groupsItems);

    return (
      <RegistrationScreenStep2
        navigation={navigation}
        loading={isLoading}
        nameValid={isNameValid}
        surnameValid={isSurnameValid}
        verificationCodeValid={isVerificationCodeValid}
        countryValid={isCountryValid}
        patronymicValid={isPatronymicValid}
        type={this.props.navigation.getParam('studentType', '')}
        sex={sex}
        group={group}
        faculty={faculty}
        groups={groupsItems}
        faculties={facultiesItems}
        name={name}
        surname={surname}
        date={date}
        verificationCode={verificationCode}
        country={countryLabel}
        countries={countries}
        patronymic={patronymic}
        setSex={this.setSex}
        onSetGroup={this.setGroup}
        onSetFaculty={this.setFaculty}
        onSetName={this.setName}
        onSetSurname={this.setSurname}
        onSetDate={this.setDate}
        onSetVerificationCode={this.setVerificationCode}
        onSetCountry={this.setCountry}
        onSetPatronymic={this.setPatronymic}
        onDone={this.onDone}
        alert={this.alert}
      />
    );
  }
}
