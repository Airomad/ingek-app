import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

import store from 'store';
import { INCORRECT_INPUT_LOGIN_OR_PASSWORD } from 'actions/errors_code';
import { signIn } from 'actions/api/signIn';
import { socialLogin } from 'actions/api/user';
import { FacebookLogin, GoogleLogin } from 'utils/social-connect';
import { setUserLoginData } from 'utils';

import SignInScreen from 'components/Auth/SignInScreen';
import Alert from 'components/Alert';


export default class SignInScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    login: 't2@t.t',
    password: '1',
    isLoading: false,
    isFormValid: true,
    errorMessage: false,
  }

  auth = () => {
    const { login, password } = this.state;
    store.dispatch(signIn(login, password));
  }

  componentDidMount() {
    const { password } = this.state;
    let currentStore = store.getState();
    let isLoading = currentStore.app.loading;
    let formErrors = currentStore.app.lastErrors;
    let isFormValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_LOGIN_OR_PASSWORD));

    let stateToUpdate = {
      isLoading,
      isFormValid,
    };

    if (!isFormValid) {
      stateToUpdate.errorMessage = "Incorrect Login or Password";
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();

      isLoading = currentStore.app.loading;
      formErrors = currentStore.app.lastErrors;
      isFormValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_LOGIN_OR_PASSWORD));
      
      stateToUpdate = {
        isLoading,
        isFormValid,
      };

      if (!isFormValid) {
        stateToUpdate.errorMessage = "Incorrect Login or Password";
      }
      this.setState(stateToUpdate);

      if (currentStore.app && currentStore.app.token && currentStore.app.login) {
        try {
          console.log('LOOGIN IS',  currentStore.app.login);
          const savedToken = setUserLoginData(currentStore.app.token, currentStore.app.login, true, password);
        } catch (error) {
          console.error('Can\'t set app token!!!');
        }
        this.unsubscribeRedux && this.unsubscribeRedux();
        this.props.navigation.navigate('app');
      }
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  signInFb = () => {
    FacebookLogin(
      (result, accessToken) => {
        console.log('Get token ', accessToken);
        store.dispatch(socialLogin('fb', accessToken));
      },
      (error) => {
        console.error(error);
      }
    );
  }

  signInGoogle = () => {
    GoogleLogin(
      (result, accessToken) => {
        console.log('Google get token');
        console.log(result);
        console.log(accessToken);
        store.dispatch(socialLogin('go', accessToken));
      },
      (error) => {
        console.error(error);
      }
    )
  }

  render() {
    const { navigation } = this.props;
    const { login, password, isLoading, isFormValid, errorMessage } = this.state;

    return (
      <SignInScreen
        navigation={navigation}
        login={login}
        password={password}
        onLoginChange={(login) => this.setState({ login })}
        onPasswordChange={(password) => this.setState({ password })}
        onLogin={this.auth}
        onSignInFb={this.signInFb}
        onSignInGoogle={this.signInGoogle}
        loading={isLoading}
        alert={this.alert}
        validationResult={{
          loginValid: isFormValid,
          passwordValid: isFormValid,
        }}
      />
    );
  }
}
