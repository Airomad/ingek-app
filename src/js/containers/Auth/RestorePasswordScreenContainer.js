import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

import store from 'store';
import { INCORRECT_INPUT_LOGIN, INCORRECT_JSON_RESPONSE_FROM_SERVER } from 'actions/errors_code';
import { restorePassword } from 'actions/api/signUp';
import { setUserLoginData } from 'utils';

import RestorePasswordScreen from 'components/Auth/RestorePasswordScreen';
import Alert from 'components/Alert';


export default class RestorePasswordScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    login: '',
    isOperationSuccess: false,
    isLoading: false,
    isFormInvalid: false,
    errorMessage: false,
  }

  restorePassword = () => {
    const { login } = this.state;
    store.dispatch(
      restorePassword(
        login,
        () => this.setState({ isOperationSuccess: true })
      )
    );
  }

  componentDidMount() {
    let currentStore = store.getState();
    let isLoading = currentStore.app.loading;
    let formErrors = currentStore.app.lastErrors;
    let isFormInvalid = false;
      
    let stateToUpdate = {
      isLoading,
      isFormInvalid,
    };

    if (formErrors) {
      isFormInvalid = true;

      if (formErrors.includes(INCORRECT_INPUT_LOGIN)) {
        stateToUpdate.errorMessage = "Incorrect Login!";
      } else if (formErrors.includes(INCORRECT_JSON_RESPONSE_FROM_SERVER) || formErrors.includes(990)) {
        stateToUpdate.errorMessage = "Server error";
      }
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();

      isLoading = currentStore.app.loading;
      formErrors = currentStore.app.lastErrors;
      isFormInvalid = false;
      
      stateToUpdate = {
        isLoading,
        isFormInvalid,
      };

      if (formErrors) {
        isFormInvalid = true;
  
        if (formErrors.includes(INCORRECT_INPUT_LOGIN)) {
          stateToUpdate.errorMessage = "Incorrect Login!";
        } else if (formErrors.includes(INCORRECT_JSON_RESPONSE_FROM_SERVER) || formErrors.includes(990)) {
          stateToUpdate.errorMessage = "Server error";
        }
      }
      this.setState(stateToUpdate);

      // if (currentStore.app && currentStore.app.token && currentStore.app.login) {
 
      //   this.unsubscribeRedux && this.unsubscribeRedux();
      //   this.props.navigation.navigate('signIn');
      // }
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }


  render() {
    const { navigation } = this.props;
    const { login, isLoading, isFormInvalid, errorMessage, isOperationSuccess } = this.state;

    return (
      <RestorePasswordScreen
        navigation={navigation}
        login={login}
        onLoginChange={(login) => this.setState({ login })}
        onRestorePassword={this.restorePassword}
        loading={isLoading}
        alert={this.alert}
        operationSuccess={isOperationSuccess}
        validationResult={{
          loginValid: !isFormInvalid,
        }}
      />
    );
  }
}
