import React, { Component } from 'react';

import store from 'store';
import { INCORRECT_INPUT_LOGIN, INCORRECT_INPUT_PASSWORD, USER_ALREADY_EXISTS } from 'actions/errors_code';
import { signUp } from 'actions/api/signUp';
import { socialLogin } from 'actions/api/user';
import { FacebookLogin } from 'utils/social-connect';

import SignUpScreen from 'components/Auth/SignUpScreen';
import Alert from 'components/Alert';


export default class SignUpScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    login: 't2@t.t',
    password: '1',
    isLoading: false,
    isLoginValid: true,
    isPasswordValid: true,
    errorMessage: false,
    isUserRegistered: false,
  }

  register = () => {
    const { login, password } = this.state;
    store.dispatch(signUp(login, password));
  }

  componentDidMount() {
    const { password } = this.state;
    let currentStore = store.getState();
    let isLoading = currentStore.app.loading;
    let formErrors = currentStore.app.lastErrors;
    let isLoginValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_LOGIN));
    let isPasswordValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_PASSWORD));
    //let isUserRegistered = currentStore.app && currentStore.app.token;

    if (currentStore.app && currentStore.app.token) {
      this.unsubscribeRedux && this.unsubscribeRedux();
      this.props.navigation.navigate('app');
    }

    let stateToUpdate = {
      isLoading,
      isLoginValid,
      isPasswordValid,
      //isUserRegistered,
    };

    if (!isLoginValid) {
      stateToUpdate.errorMessage = "Email can't be empty";
    }
    if (!isPasswordValid) {
      stateToUpdate.errorMessage = "Password can't be empty";
    }
    if (formErrors && formErrors.includes(USER_ALREADY_EXISTS)) {
      stateToUpdate.errorMessage = "This user is already registered!";
    }
    this.setState(stateToUpdate);

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();

      isLoading = currentStore.app.loading;
      formErrors = currentStore.app.lastErrors;
      isLoginValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_LOGIN));
      isPasswordValid = !(formErrors && formErrors.includes(INCORRECT_INPUT_PASSWORD));
      //isUserRegistered = currentStore.app && currentStore.app.token;
      
      stateToUpdate = {
        isLoading,
        isLoginValid,
        isPasswordValid,
        //isUserRegistered,
      };

      if (!isLoginValid) {
        stateToUpdate.errorMessage = "Email can't be empty";
      }
      if (!isPasswordValid) {
        if (!isLoginValid) {
          stateToUpdate.errorMessage = "Email and Password can't be empty";
        } else {
          stateToUpdate.errorMessage = "Password can't be empty";
        }
      }
      if (formErrors && formErrors.includes(USER_ALREADY_EXISTS)) {
        stateToUpdate.errorMessage = "This user is already registered!";
      }
      this.setState(stateToUpdate);

      if (currentStore.app && currentStore.app.token) {
        this.unsubscribeRedux && this.unsubscribeRedux();
        this.props.navigation.navigate('registerSetInfo');
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  signUpFb = () => {
    FacebookLogin(
      (result, accessToken) => {
        console.log('Get token ', accessToken);
        store.dispatch(socialLogin('fb', accessToken));
      },
      (error) => {
        console.error(error);
      }
    );
  }

  render() {
    const { navigation } = this.props;
    const { login, password, isLoading, isLoginValid, isPasswordValid, errorMessage } = this.state;

    return (
      <SignUpScreen
        login={login}
        password={password}
        onLoginChange={(login) => this.setState({ login })}
        onPasswordChange={(password) => this.setState({ password })}
        navigation={navigation}
        onRegister={this.register}
        loading={isLoading}
        alert={this.alert}
        onSignUpFb={this.signUpFb}
        validationResult={{
          loginValid: isLoginValid,
          passwordValid: isPasswordValid,
        }}
      />
    );
  }
}
