import React, { Component } from 'react';

import store from 'store';
import { updateUserTempPhoto, updateUserInfo, getFaculties, getGroups } from 'actions/api/user';

import {
  INCORRECT_JSON_RESPONSE_FROM_SERVER,
} from 'actions/errors_code';

import ProfileEditScreen from 'components/Profile/ProfileEditScreen';
import Alert from 'components/Alert';

import { getGroupsByFaculty } from 'utils';

export default class ProfileEditScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    token: null,
    userData: null,
    image: '',
    name: '',
    surname: '',
    role: '',
    email: '',
    password: '',
    sex: '',
    age: '',
    faculty: '',
    group: '',
    photoSourceToUpdate: null,
    photoHash: null,
    isPhotoUploading: false,
    isDataUpdating: false,
    errorMessage: null,
    faculties: null,
    groups: null,
  };

  prepareUserData = (userData) => ({
    image: userData && userData.photo ? userData.photo : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg',
    name: userData && userData.fname ? userData.fname : '',
    surname: userData && userData.lname ? userData.lname : '',
    role: userData && userData.type ? userData.type : 'not detected',
    email: userData && userData.login ? userData.login : '',
    password: this.state.password,
    sex: userData && userData.sex ? userData.sex : 'not detected',
    age: userData && userData.age ? userData.age : 'not detected',
    faculty: userData && userData.faculty ? userData.faculty : 'not detected',
    group: userData && userData.group ? userData.group : 'not detected',
  });

  componentDidMount() {
    const { navigation } = this.props;

    store.dispatch(getFaculties());
    store.dispatch(getGroups());

    let currentStore = store.getState();
    let token = currentStore.app && currentStore.app.token;
    let password = currentStore.app && currentStore.app.password;
    let faculties = currentStore.user && currentStore.user.faculties;
    let groups = currentStore.user && currentStore.user.groups;
    let userData = currentStore.user && currentStore.user.userData;
    let photoHash = currentStore.user && currentStore.user.photoHash;
    let photoIsUploading = currentStore.user && currentStore.user.photoIsUploading;
    let lastErrors = currentStore.app && currentStore.app.lastErrors;
    let isLoading = currentStore.app && currentStore.app.loading;

    stateToUpdate = {};

    if (groups !== this.state.groups) {
      const g = [];
      groups.map((item, index) => {
        const newItem = {
          label: item.group_name,
          value: item.group_id,
        }
        g.push(newItem);
      });
      stateToUpdate.groups = g;
    }
    if (faculties !== this.state.faculties) {
      const f = [];
      faculties.map((item, index) => {
        const newItem = {
          label: item.faculty_id,
          value: item.faculty_name,
        }
        f.push(newItem);
      })
      stateToUpdate.faculties = f;
    }

    if (userData && userData !== this.state.userData) {
      stateToUpdate.userData = userData;
      const preparedUserData = this.prepareUserData(userData);
      stateToUpdate = {
        ...stateToUpdate,
        ...preparedUserData,
      };
    }
    if (token && token !== this.state.token) {
      stateToUpdate.token = token;
    }
    if (password !== this.state.password) {
      stateToUpdate.password = password;
    }
    if (photoHash !== this.state.photoHash) {
      stateToUpdate.photoHash = photoHash;
    }
    if (lastErrors && !this.state.errorMessage) {
      if (lastErrors === INCORRECT_JSON_RESPONSE_FROM_SERVER || lastErrors.includes(INCORRECT_JSON_RESPONSE_FROM_SERVER)) {
        stateToUpdate.errorMessage = 'Server error';
      }
    }
    if (this.state.isLoading !== isLoading) {
      stateToUpdate.isLoading = isLoading;
    }
    if (photoIsUploading !== this.state.isPhotoUploading) {
      stateToUpdate.isPhotoUploading = photoIsUploading;
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      token = currentStore.app && currentStore.app.token;
      password = currentStore.app && currentStore.app.password;
      groups = currentStore.user && currentStore.user.groups;
      faculties = currentStore.user && currentStore.user.faculties;
      userData = currentStore.user && currentStore.user.userData;
      photoHash = currentStore.user && currentStore.user.photoHash;
      photoIsUploading = currentStore.user && currentStore.user.photoIsUploading;
      lastErrors = currentStore.app && currentStore.app.lastErrors;
      isLoading = currentStore.app && currentStore.app.loading;

      stateToUpdate = {};

      if (groups !== this.state.groups) {
        const g = [];
        groups.map((item, index) => {
          const newItem = {
            label: item.group_name,
            value: item.group_id,
          }
          g.push(newItem);
        });
        stateToUpdate.groups = g;
      }
      if (faculties !== this.state.faculties) {
        const f = [];
        faculties.map((item, index) => {
          const newItem = {
            label: item.faculty_name,
            value: item.faculty_id,
          }
          f.push(newItem);
        })
        stateToUpdate.faculties = f;
      }
      if (userData && userData !== this.state.userData) {
        stateToUpdate.userData = userData;

        const preparedUserData = this.prepareUserData(userData);
        stateToUpdate = {
          ...stateToUpdate,
          ...preparedUserData,
        };

        if (this.state.isDataUpdating) {
          this.unsubscribeRedux && this.unsubscribeRedux();
          navigation.navigate('profile');
        }
      }
      if (token && token !== this.state.token) {
        stateToUpdate.token = token;
      }
      if (password !== this.state.password) {
        stateToUpdate.password = password;
      }
      if (photoHash !== this.state.photoHash) {
        stateToUpdate.photoHash = photoHash;
      }
      if (lastErrors && !this.state.errorMessage) {
        if (lastErrors === INCORRECT_JSON_RESPONSE_FROM_SERVER || lastErrors.includes(INCORRECT_JSON_RESPONSE_FROM_SERVER)) {
          stateToUpdate.errorMessage = 'Server error';
        }
      }
      if (this.state.isLoading !== isLoading) {
        stateToUpdate.isLoading = isLoading;
      }
      if (photoIsUploading !== this.state.isPhotoUploading) {
        stateToUpdate.isPhotoUploading = photoIsUploading;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  prepareNewPhotoSource = (source) => {
    // this.setState({ photoSourceToUpdate: source });
    
    const { token } = this.state;

    if (source && token) {
      store.dispatch(updateUserTempPhoto(source, token));
    }
  }

  // changeUserData = (userData) => {
  //   console.log(userData);
  //   if (userData !== this.state.userData) {
  //     this.setState({ userData });
  //   }
  // }

  changeUserData = (field, value) => {
    if (field === 'password') {
      this.setState({ password: value });
    } else {
      const userData = {
        ...this.state.userData,
        [field]: value,
      };
      console.log(userData);
      this.setState({ userData, [field]: value });
    }
  }

  applyChangeUserData = () => {
    const { token, photoHash, userData, password } = this.state;

    if (token) {
      this.setState({ isDataUpdating: true });
      store.dispatch(updateUserInfo(token, { ...userData, password }, photoHash));
    }
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  getGroupValue = (group) => {
    if (this.state.groups && this.state.groups.length > 0) {
      const f = this.state.groups.filter(item => item.value === group);
      if (f && f[0]) {
        return { group: f[0].label, groupId: group };
      }
    }
    return null;
  };

  getFacultyValue = (faculty) => {
    if (this.state.faculties && this.state.faculties.length > 0) {
      const f = this.state.faculties.filter(item => item.value === faculty);
      if (f && f[0]) {
        return { faculty: f[0].label, facultyId: faculty };
      }
    }
    return null;
  };

  render() {
    const { navigation } = this.props;
    const { userData, isPhotoUploading, isLoading, faculties, groups, password } = this.state;
    const updateUserData = {
      ...userData,
      password,
    };

    if (groups) {
      console.log(groups);
      getGroupsByFaculty(groups, 1);
    }

    //console.log('SKA', updateUserData);
    //console.log(this.state.photoHash);

    // const data = {
    //   image: userData && userData.photo ? userData.photo : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg',
    //   fname: userData && userData.fname ? userData.fname : '',
    //   lname: userData && userData.lname ? userData.lname : '',
    //   role: userData && userData.type ? userData.type : 'not detected',
    //   email: userData && userData.login ? userData.login : '',
    //   password: 'something',
    //   sex: userData && userData.sex ? userData.sex : 'not detected',
    //   age: userData && userData.age ? userData.age : 'not detected',
    //   faculty: userData && userData.faculty ? userData.faculty : 'not detected',
    //   group: userData && userData.group ? userData.group : 'not detected',
    // };

    //const { image, name, surname, role, email, password, sex, age, faculty, group } = this.state;

    return (
      <ProfileEditScreen
        navigation={navigation}
        // image={image}
        // name={name}
        // surname={surname}
        // role={role}
        // email={email}
        // password={password}
        // sex={sex}
        // age={age}
        // faculty={faculty}
        // group={group}
        faculties={faculties}
        groups={groups}
        data={updateUserData}
        alert={this.alert}
        loading={isLoading}
        photoUploading={isPhotoUploading}
        onChangeUserData={this.changeUserData}
        onApplyChangeUserData={this.applyChangeUserData}
        onPrepareNewPhotoSource={this.prepareNewPhotoSource}
      />
    );
  }
}
