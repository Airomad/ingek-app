import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import store from 'store';

import { loadFeedData } from 'actions/api/feed';

import FeedScreen from 'components/Feed/FeedScreen';
import Alert from 'components/Alert';


const Wrapper = styled.View`
  flex: 1;
  background-color: white;
  justify-content: center;
  align-items: center;
`;

const Label = styled.Text`
  color: black;
  font-size: 20px;
`;


export default class FeedScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  state = {
    errorMessage: 'Firbase service is offline!',
    data: null,
    loading: false,
    loadedAllData: false,
  };

  componentDidMount() {
    this.refreshPage();

    let currentStore = store.getState();
    let loading = currentStore.app && currentStore.app.loading;
    let data = currentStore.feed && currentStore.feed.data;
    let loadedAllData = currentStore.feed && currentStore.feed.loadedAllData;

    let stateToUpdate = {};

    if (data !== this.state.data) {
      stateToUpdate.data = data;
    }
    if (loading !== this.state.loading) {
      stateToUpdate.loading = loading;
    }
    if (loadedAllData !== this.state.loadedAllData) {
      stateToUpdate.loadedAllData = loadedAllData;
    }

    this.unsubscribeRedux = store.subscribe(() => {
      currentStore = store.getState();
      loading = currentStore.app && currentStore.app.loading;
      data = currentStore.feed && currentStore.feed.data;
      loadedAllData = currentStore.feed && currentStore.feed.loadedAllData;

      if (data !== this.state.data) {
        stateToUpdate.data = data;
      }
      if (loading !== this.state.loading) {
        stateToUpdate.loading = loading;
      }
      if (loadedAllData !== this.state.loadedAllData) {
        stateToUpdate.loadedAllData = loadedAllData;
      }

      this.setState(stateToUpdate);
    });

    this.setState(stateToUpdate);
  }

  componentWillUnmount() {
    this.unsubscribeRedux && this.unsubscribeRedux();
  }

  refreshPage = () => {
    store.dispatch(loadFeedData(true));
  }

  loadMoreData = (bottomScrollOffset) => {
    console.log('reched', bottomScrollOffset);
    const { data, loading } = this.state;
    if (!loading) {
      const currentOffset = (data && data.length) || 0;
      store.dispatch(loadFeedData(false, currentOffset));
    }
  }

  alert = () => {
    const { errorMessage } = this.state;
    
    if (errorMessage) {
      return (
        <Alert
          message={errorMessage}
          onDisapear={() => this.setState({ errorMessage: null })}
          delay={5000}
        />
      );
    } else return null;
  }

  render() {
    const { navigation } = this.props;
    const { loading, data, loadedAllData } = this.state;

    return (
      <FeedScreen
        navigation={navigation}
        onRefreshPage={this.refreshPage}
        onLoadMoreData={this.loadMoreData}
        loading={loading}
        isLoadedAllData={loadedAllData}
        data={data}
        alert={this.alert}
      />
    );
  }
}
