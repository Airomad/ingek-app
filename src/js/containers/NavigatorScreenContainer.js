import React, { Component } from 'react';
import { PixelRatio } from 'react-native';
import styled from 'styled-components/native';

import NavigatorScreen from 'components/Navigator/NavigatorScreen';


export default class NavigatorScreenContainer extends Component {
  static navigationOptions = {
    headerStyle: {
      height: 0,
    },
  };

  render() {
    const { navigation } = this.props;

    return (
      <NavigatorScreen
        navigation={navigation}
      />
    );
  }
}
