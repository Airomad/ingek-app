import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import reducers from 'reducers';

import initialState from './initialState.json';

//import { requestKey, requestCityList } from 'actions/api/utils.js';

/* eslint-disable no-underscore-dangle  */
// eslint-disable-next-line no-undef
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  initialState,
  composeEnhancers(applyMiddleware(thunk)),
);
/* eslint-enable */

// export function preloadStore() {
//   store
//     .dispatch(requestKey())
//     .then(() => store.dispatch(requestCityList()))
//     .then(() => store.dispatch(push('/main')));
// }

export default store;
