import * as langDataEn from 'data/lang-english.json';
import * as langDataRu from 'data/lang-russian.json';
import * as langDataUk from 'data/lang-ukrainian.json';


class Dictionary {
  constructor() {
    this.changeLanguage('en');
  }

  changeLanguage(newLangCode) {
    this._activeLangCode = newLangCode;
    this._data = this.getDataByLangCode(this._activeLangCode);
  }

  getDataByLangCode(langCode) {
    switch (langCode) {
      case 'en': return langDataEn;
      case 'ru': return langDataRu;
      case 'uk': return langDataUk;
      default: return langDataEn;
    }
  }

  getActiveLangCode() {
    return this._activeLangCode;
  }

  get(word) {
    if (this._data && this._data.words[word]) {
      return this._data.words[word];
    } else if (langDataEn.words[word]) {
      return langDataEn.words[word];
    } else return 'NO_TRANSLATION';
  }
}

const dictionary = new Dictionary();

export default dictionary;