import FBSDK from 'react-native-fbsdk';
const {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
  AccessToken
} = FBSDK;

import GoogleSignIn from 'react-native-google-sign-in';


export async function GoogleLogin(onSuccess, onError) {
  await GoogleSignIn.configure({
    // iOS
    clientID: 'AIzaSyAnoodggiTIgsBLAKeKhFFs7lzKk9_AF8Y',
 
    // iOS, Android
    // https://developers.google.com/identity/protocols/googlescopes
    scopes: ['https://www.googleapis.com/auth/drive.readonly'],
 
    // iOS, Android
    // Whether to request email and basic profile.
    // [Default: true]
    // // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a06bf16b507496b126d25ea909d366ba4
    // shouldFetchBasicProfile: true,
 
    // // iOS
    // // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a486c8df263ca799bea18ebe5430dbdf7
    // language: 'en',
 
    // // iOS
    // // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd
    // loginHint: 'LOGIN HINT BABE',
 
    // iOS, Android
    // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#ae214ed831bb93a06d8d9c3692d5b35f9
    serverClientID: '243292378563-ul5hkasvgcvqmvl6sc8jmfpfnfb5uavf.apps.googleusercontent.com',
 
    // Android
    // Whether to request server auth code. Make sure to provide `serverClientID`.
    // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#requestServerAuthCode(java.lang.String, boolean)
    // offlineAccess: true,
    
    // // Android
    // // Whether to force code for refresh token.
    // // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#requestServerAuthCode(java.lang.String, boolean)
    // forceCodeForRefreshToken: false,
 
    // // iOS
    // // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a211c074872cd542eda53f696c5eef871
    // openIDRealm: 'OPEN ID REALM BABE',
 
    // // Android
    // // https://developers.google.com/android/reference/com/google/android/gms/auth/api/signin/GoogleSignInOptions.Builder.html#setAccountName(java.lang.String)
    // //accountName: '',
 
    // // iOS, Android
    // // https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a6d85d14588e8bf21a4fcf63e869e3be3
    // hostedDomain: 'com.ingek',
  });
 
  const user = await GoogleSignIn.signInPromise();
 
  if (user) {
    const { userID, email, accessToken, givenName, familyName, photoUrlTiny } = user;
    
    onSuccess && onSuccess(user, accessToken);
  } else {
    // TODO:
    console.error('Error in Google');
    onError && onError('Error in Google');
  }
}

export function FacebookLogin(onSuccess, onError) {
  LoginManager.logInWithReadPermissions(['public_profile']).then(
    function(result) {
      if (result.isCancelled) {
        //alert('Login cancelled');
        // TODO: handle this;
        
        onError && onError('Login is canceled');

      } else {
        console.log(result);
        // alert('Login success with permissions: '
        //   +result.grantedPermissions.toString());
        
        console.log('fethcing data..');

        AccessToken.getCurrentAccessToken().then(
          (data) => {
            const { accessToken } = data;
            //EAAGL4HlGoqIBAMX4hW3SLyI3zbb5iqGlAh1U2HhyrhDqAjvZAANBRD5V42cMF2ahCPkcTZAIUCgOISqVfsyFMOaPfwqUMaVRGP0ZC32yQKlmM3RwfKY3YvtpZBlAYIr4gYWLk037V0SPSS2jF29hCMWCMkBZAF8ZBb6TkUjW5EkwTDLvRae4kbEfPlPtgpKibQnocT6Q4oYJ2KksWukPF57hXpA247Sd2ZCPuF0YswwyiIRXvZAhZA0yPe5ulbZAJwpNUZD
            const infoRequest = new GraphRequest(
              '/me?fields=email,name,picture',
              null,
              (error, result) => {
                if (error) {
                  console.log('Error fetching data: ', error);
                } else {
                  console.log('Success fetching data: ', result);
                  onSuccess && onSuccess(result, accessToken);
                }
              }
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          }
        ).catch(error => console.log(error));
      }
    },
    function(error) {
      alert('Login fail with error: ' + error);
    }
  );
}
