import { AsyncStorage } from 'react-native';


// Auth

const userLoginDataKey = '@ingek:userLoginData';

export async function getUserLoginData() {
  const userLoginDataStr = await AsyncStorage.getItem(userLoginDataKey);
  if (userLoginDataKey) {
    // console.log('GET STRING OF USER DATA ', userLoginDataStr);
    return JSON.parse(userLoginDataStr);
  } else {
    return null;
  }
}

export async function setUserLoginData(token, login, isDebug, password = '') {
  // console.log('Saving user login data');
  // console.log('Login is', login);
  // console.log('Token is', token);
  const userLoginData = JSON.stringify({
    login,
    token,
    password,
  });
  await AsyncStorage.setItem(userLoginDataKey, userLoginData);
  
  if (isDebug) {
    console.log('User login data was set!');
    return await AsyncStorage.getItem(userLoginDataKey);
  }
}

export async function clearUserLoginData(isDebug) {
  await AsyncStorage.removeItem(userLoginDataKey);
  
  if (isDebug) {
    console.log('User login data was removed!');
    return await AsyncStorage.getItem(userLoginDataKey);
  }
}

// Settings

const settingsKey = '@ingek:settings';
const INIT_SETTINGS = {
  language: 'en',
  notificationsEnabled: true,
};

export async function getSettings() {
  const settingsStr = await AsyncStorage.getItem(settingsKey);
  if (settingsStr) {
    return JSON.parse(settingsStr);
  } else {
    return INIT_SETTINGS;
  }
}

export async function saveSettings(settingsData) {
  if (settingsData) {
    console.log('save settings data', settingsData);
    AsyncStorage.setItem(settingsKey, JSON.stringify(settingsData));
  } else {
    AsyncStorage.setItem(settingsKey, INIT_SETTINGS);
  }
}

// Other

export function getGroupsByFaculty(groups, facultyId) {
  if (groups) {
    const filteredGroups = groups.filter(item => item.faculty_id === facultyId);
    if (filteredGroups) {
      console.log('Filtered groups ', filteredGroups);
    }
  }
  return [];
}

// Schedule

const scheduleRequestsCacheKey = '@ingek:scheduleRequestsCache';

export async function getScheduleRequestsCache() {
  const scheduleRequestsCacheStr = await AsyncStorage.getItem(scheduleRequestsCacheKey);
  if (scheduleRequestsCacheStr) {
    return JSON.parse(scheduleRequestsCacheStr);
  } else {
    return null;
  }
}

export async function saveScheduleRequestCache(request) {
  const currentCache = getScheduleRequestsCache();
  if (request) {
    const newCache = [...currentCache, ...request];
    AsyncStorage.setItem(scheduleRequestsCacheKey, JSON.stringify(newCache));
  }
}