import {
  INCORRECT_INPUT_SEX,
  INCORRECT_INPUT_NAME,
  INCORRECT_INPUT_SURNAME,
  INCORRECT_INPUT_FACULTY,
  INCORRECT_INPUT_GROUP,
  INCORRECT_INPUT_USER_TYPE,
  INCORRECT_INPUT_VERIFICATION_CODE,
  INCORRECT_INPUT_COUNTRY,
  INCORRECT_INPUT_PATRONYMIC,
} from 'actions/errors_code';

export function getLastErrorMessageByCodes(errors) {
  if (errors && errors.length > 0) {
    return getErrorMessageByCode(errors[0]);
  } else {
    return null;
  }
}

export function isErrorCodeHandled(errors, errorCode) {
  if (errors && errors.length > 0 && errorCode) {
    if (errors.includes(errorCode)) {
      return true;
    }
  }
  return false;
}

export function getErrorMessageByCode(errorCode) {
  switch (errorCode) {
    case INCORRECT_INPUT_FACULTY: return "Incorrect faculty!";
    case INCORRECT_INPUT_GROUP: return "Incorrect group!";
    case INCORRECT_INPUT_SURNAME: return "Incorrect second name!";
    case INCORRECT_INPUT_NAME: return "Incorrect first name!";
    case INCORRECT_INPUT_SEX: return "Incorrect sex!";
    case INCORRECT_INPUT_USER_TYPE: return "Incorrect user type!";
    case INCORRECT_INPUT_COUNTRY: return "Incorrect country!";
    case INCORRECT_INPUT_PATRONYMIC: return "Incorrect patronymic!";
    case INCORRECT_INPUT_VERIFICATION_CODE: return "Incorrect teacher verification code!";
    default: return null;
  }
}