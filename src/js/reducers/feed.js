import {
  API_SET_FEED_INIT_DATA,
  API_SET_FEED_EXTRA_DATA,
  API_SET_FEED_TOTAL_COUNT,
} from 'actions/types';

const INIT_STATE = {
  data: null,
  totalCount: 0,
  loadedAllData: false,
};

export default function feed(state = INIT_STATE, action) {
  switch (action.type) {
    case API_SET_FEED_INIT_DATA:
      return {
        ...state,
        data: action.data,
        loadedAllData: false,
      };
    case API_SET_FEED_EXTRA_DATA:
    const data = state.data ? [...state.data, ...action.data] : action.data;
    const loadedAllData = action.data && action.data.length > 0 ? false : true;
      return {
        ...state,
        data,
        loadedAllData,
      };
    case API_SET_FEED_TOTAL_COUNT:
      return {
        ...state,
        totalCount: action.totalCount,
      };
    default:
      return state;
  }
}
