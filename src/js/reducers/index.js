import { combineReducers } from 'redux';

import app from './app';
import registration from './registration';
import news from './news';
import user from './user';
import search from './search';
import feed from './feed';
import schedule from './schedule';

export default combineReducers({
  app,
  registration,
  news,
  user,
  search,
  feed,
  schedule,
});
