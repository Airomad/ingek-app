import {
  API_SET_SCHEDULE_DATA,
} from 'actions/types';

const INIT_STATE = {
  data: null,
};

export default function schedule(state = INIT_STATE, action) {
  switch (action.type) {
    case API_SET_SCHEDULE_DATA:
      return {
        ...state,
        data: action.data,
      };
    default:
      return state;
  }
}
