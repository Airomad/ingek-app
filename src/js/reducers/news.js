import {
  API_SET_NEWS_INIT_DATA,
  API_SET_NEWS_EXTRA_DATA,
  API_SET_NEWS_VIEW,
} from 'actions/types';

const INIT_STATE = {
  data: [],
  newsViewData: null,
  loadedAllData: false,
};

export default function news(state = INIT_STATE, action) {
  switch (action.type) {
    case API_SET_NEWS_INIT_DATA:
      return {
        ...state,
        data: action.data,
        loadedAllData: false,
      };
    case API_SET_NEWS_EXTRA_DATA:
      const data = state.data ? [...state.data, ...action.data] : action.data;
      const loadedAllData = action.data && action.data.length > 0 ? false : true;
      return {
        ...state,
        data,
        loadedAllData,
      };
    case API_SET_NEWS_VIEW:
      return {
        ...state,
        newsViewData: action.data,
      };
    default:
      return state;
  }
}
