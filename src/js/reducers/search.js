import {
  API_SET_SEARCH_AUTOCOMPLETE_DATA,
} from 'actions/types';

const INIT_STATE = {
  autoCompleteData: null,
};

export default function search(state = INIT_STATE, action) {
  switch (action.type) {
    case API_SET_SEARCH_AUTOCOMPLETE_DATA:
      return {
        ...state,
        autoCompleteData: action.data,
      };
    default:
      return state;
  }
}
