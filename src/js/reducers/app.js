import {
  LOADING_START,
  LOADING_END,
  INCORECT_INPUT_DATA,
  API_SET_AUTH_DATA,
  API_SET_NEWS_INIT_DATA,
  API_SET_NEWS_EXTRA_DATA,  
  API_SET_NEWS_VIEW,
  API_SET_SEARCH_AUTOCOMPLETE_DATA,
  API_SET_USER_DATA,
  API_SET_USER_FACULTIES,
  API_SET_USER_GROUPS,
  API_SET_FEED_INIT_DATA,
  API_SET_FEED_EXTRA_DATA,
  API_SET_FEED_TOTAL_COUNT,
  API_SET_SCHEDULE_DATA,
  SERVER_RETURNED_BAD_JSON,
  SERVER_INTERNAL_ERROR,
  API_SET_SETTINGS,
} from 'actions/types';

const INIT_STATE = {
  loading: false,
  lastErrors: null,
  token: null,
  login: null,
  password: null,
  settingsData: null,
};

export default function app(state = INIT_STATE, action) {
  switch (action.type) {
    case LOADING_START:
      return {
        ...state,
        lastErrors: null,
        loading: true,
      };
    case API_SET_NEWS_INIT_DATA: stopLoading(state);
    case API_SET_NEWS_EXTRA_DATA: stopLoading(state);
    case API_SET_NEWS_VIEW: stopLoading(state);
    case LOADING_END: stopLoading(state);
    case API_SET_USER_FACULTIES: stopLoading(state);
    case API_SET_USER_GROUPS: stopLoading(state);
    case API_SET_USER_DATA: stopLoading(state);
    case API_SET_SEARCH_AUTOCOMPLETE_DATA: stopLoading(state);
    case API_SET_FEED_INIT_DATA: stopLoading(state);
    case API_SET_FEED_EXTRA_DATA: stopLoading(state);
    case API_SET_FEED_TOTAL_COUNT: stopLoading(state);
    case API_SET_SCHEDULE_DATA: stopLoading(state);
    
    case INCORECT_INPUT_DATA:
      return {
        ...state,
        lastErrors: action.lastErrors,
        loading: false,
      };
    case SERVER_RETURNED_BAD_JSON:
      return {
        ...state,
        lastErrors: action.lastErrors,
        loading: false,
      };
    case SERVER_INTERNAL_ERROR:
      return {
        ...state,
        lastErrors: action.lastErrors,
        loading: false,
      };
    case API_SET_AUTH_DATA: // Move it to user reducer and make loading end on each API_SET_* calling
      return {
        ...state,
        token: action.token,
        login: action.login,
        password: action.password,
        lastErrors: null,
        loading: false,
      };
    case API_SET_SETTINGS:
      return {
        ...state,
        settingsData: action.settingsData,
      };
    default:
      return state;
  }
}

function stopLoading(state) {
  return {
    ...state,
    loading: false,
  }
}
