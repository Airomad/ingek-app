import {
  REG_SET_STUDENT_TYPE,
  REG_SET_SEX,
  SIGN_UP_SET_FILLING_INFO,
} from 'actions/types';

const INIT_STATE = {
  studentType: '',
  sex: '',
  faculties: [],
  groups: [],
  registered: false,
};

export default function registration(state = INIT_STATE, action) {
  switch (action.type) {
    case REG_SET_STUDENT_TYPE:
      return {
        ...state,
        studentType: action.studentType,
      };
    case REG_SET_SEX:
      return {
        ...state,
        sex: action.sex,
      };
    case SIGN_UP_SET_FILLING_INFO:
      return {
        ...state,
        registered: true,
      };
    default:
      return state;
  }
}
