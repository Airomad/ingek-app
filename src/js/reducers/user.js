import {
  API_SET_USER_DATA,
  API_SET_USER_TEMP_PHOTO,
  API_SET_USER_PHOTO_HASH,
  API_USER_DATA_UPDATE_START,
  API_SET_USER_FACULTIES,
  API_SET_USER_GROUPS,
} from 'actions/types';

const INIT_STATE = {
  userData: null,
  photoHash: null,
  photoIsUploading: false,
  needToUpdate: false,
  faculties: [],
  groups: [],
};

export default function user(state = INIT_STATE, action) {
  switch (action.type) {
    case API_SET_USER_DATA:
      const { data } = action;
      const userData = {
        photo: data.photo ? (data.photo + '?r=' + new Date().getTime()) : 'https://ideanto.com/wp-content/uploads/2013/07/foto-perfil.jpg',
        fname: data.fname ? data.fname : '',
        lname: data.lname ? data.lname : '',
        type: data.type ? data.type : '',
        login: data.login ? data.login : '',
        password: data.password ? data.password : '',
        sex: data.sex ? data.sex : '',
        date_born: data.date_born ? data.date_born : '',
        faculty: data.faculty ? data.faculty : '',
        group: data.group ? data.group : '',
      };
      return {
        ...state,
        userData,
        needToUpdate: false,
        photoIsUploading: false,
      };
  case API_SET_USER_FACULTIES:
    return {
      ...state,
      faculties: action.faculties,
    };
  case API_SET_USER_GROUPS:
    return {
      ...state,
      groups: action.groups,
    };
    case API_USER_DATA_UPDATE_START:
      return {
        ...state,
        needToUpdate: false,
      };
    case API_SET_USER_TEMP_PHOTO:
      return {
        ...state,
        photoIsUploading: true,
      };
    case API_SET_USER_PHOTO_HASH:
      return {
        ...state,
        photoHash: action.photoHash,
        needToUpdate: action.photoHash ? true : false,
        photoIsUploading: false,
      };
    default:
      return state;
  }
}
